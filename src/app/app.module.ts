import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { VentasHoraComponent } from './pages/ventas-hora/ventas-hora.component';
import { VentasMeseroComponent } from './pages/ventas-mesero/ventas-mesero.component';
import { VentasPeriodoComponent } from './pages/ventas-periodo/ventas-periodo.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { CabezeraComponent } from './components/cabezera/cabezera.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { VentasCategoriaComponent } from './pages/ventas-categoria/ventas-categoria.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
      AppComponent,
      HeaderComponent,
      CabezeraComponent,
      HomeComponent,
      LoginComponent,
      ReportesComponent,
      VentasHoraComponent,
      VentasMeseroComponent,
      VentasPeriodoComponent,
      VentasCategoriaComponent,
      PerfilComponent,
    ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
