import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { VentasCategoriaComponent } from './pages/ventas-categoria/ventas-categoria.component';
import { VentasHoraComponent } from './pages/ventas-hora/ventas-hora.component';
import { VentasMeseroComponent } from './pages/ventas-mesero/ventas-mesero.component';
import { VentasPeriodoComponent } from './pages/ventas-periodo/ventas-periodo.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'reportes',
    component: ReportesComponent
  },
  {
    path: 'ventas-hora',
    component: VentasHoraComponent
  },
  {
    path: 'ventas-mesero',
    component: VentasMeseroComponent
  },
  {
    path: 'ventas-periodo',
    component: VentasPeriodoComponent
  },
  {
    path: 'ventas-categoria',
    component: VentasCategoriaComponent
  },
  {
    path: 'perfil',
    component: PerfilComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
