import { Injectable } from '@angular/core';
import { dataResumenGeneral } from 'src/assets/data/dataResumenGeneral';
import { ResumenGeneralModel } from '../model/chartResumenGeneralModel';
import * as moment from 'moment/moment';

@Injectable({
  providedIn: 'root'
})
export class ResumenGeneralService {

  constructor() { }

  public async getAll(): Promise<ResumenGeneralModel[]> {
    return dataResumenGeneral;
  }

  public async getVentasByTwoDates( dateFrom: Date, dateTo: Date ): Promise<ResumenGeneralModel[]> {
    let all =  await this.getAll();
    return all.filter( data => {
      let parts: string[] = data.fecha.split("/")
      let date = new Date( Number(parts[2]) , Number(parts[1])  - 1, Number(parts[0]))
      return date >= dateFrom && date <= dateTo
    })
  }
}
