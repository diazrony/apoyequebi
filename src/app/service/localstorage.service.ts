import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

  setUser( userName: string ): void {
    window.localStorage.setItem('username','');
    window.localStorage.setItem('username',userName);
  }

  getUser(): string {
    return window.localStorage.getItem('username');
  }
}
