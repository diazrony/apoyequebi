import { TestBed } from '@angular/core/testing';

import { ResumenGeneralService } from './resumen-general.service';

describe('ResumenGeneralService', () => {
  let service: ResumenGeneralService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResumenGeneralService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
