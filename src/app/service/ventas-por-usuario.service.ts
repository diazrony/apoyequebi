import { Injectable } from '@angular/core';
import { ventasPorUsuario } from 'src/assets/data/dataVentasPorUsuario';
import { VentasPorUsuarioModel } from '../model/chartVentasPorUsuarioModel';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VentasPorUsuarioService {

  constructor() { }

  public async getAll(): Promise<VentasPorUsuarioModel[]> {
    return ventasPorUsuario;
  }

  public async getVentasByName( name: string ): Promise<VentasPorUsuarioModel[]> {
    return await this.getAll().then( ventas => {
        return ventas.filter(venta => venta.nombre === name);
    })
  }

  public async getVentasTwoDates( name: string , dateFrom: Date, dateTo: Date ): Promise<any> {
      const ventaUsuario: VentasPorUsuarioModel[] = await this.getVentasByName(name);
      const ventasFechas = ventaUsuario.filter( venta => new Date(venta.fecha) >= dateFrom && new Date(venta.fecha) <= dateTo );
      return ventasFechas;
  }
}
