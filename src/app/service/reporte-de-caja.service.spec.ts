import { TestBed } from '@angular/core/testing';

import { ReporteDeCajaService } from './reporte-de-caja.service';

describe('ReporteDeCajaService', () => {
  let service: ReporteDeCajaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReporteDeCajaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
