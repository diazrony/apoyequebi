import { TestBed } from '@angular/core/testing';

import { VentasPorUsuarioService } from './ventas-por-usuario.service';

describe('VentasPorUsuarioService', () => {
  let service: VentasPorUsuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VentasPorUsuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
