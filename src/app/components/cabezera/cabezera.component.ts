import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-cabezera',
  templateUrl: './cabezera.component.html',
  styleUrls: ['./cabezera.component.scss'],
})
export class CabezeraComponent implements OnInit {

  constructor(private menu: MenuController) { }

  ngOnInit() {}

  toggle() {
    this.menu.toggle();
  }

}
