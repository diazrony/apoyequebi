export interface ChartReporteDeCajaModel {

}

export interface ReporteDeCajaModel {
  fecha: string;
  concepto: string;
  totalTransacciones: number;
  efectivo: number;
  tarjetaCredito: number;
  tarjetaDebito: number;
  total: number;
}
