export interface VentasPorUsuarioModel {
  fecha: string;
  nombre: string;
  ventas: string;
}
export interface VentasPorUsuarioChart {
  name: string;
  series: Series[];
}
export interface Series {
  name: string;
  value: string;
}
