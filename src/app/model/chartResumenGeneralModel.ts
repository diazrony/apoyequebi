export interface ResumenGeneralModel {
  fecha: string;
  operaciones: string;
  subtotal: string;
  descuento:  string;
  propina: string;
  impuestos: string;
  total: string;
}
export interface ResumenGeneralChart {
  name: string;
  value: number;
}
