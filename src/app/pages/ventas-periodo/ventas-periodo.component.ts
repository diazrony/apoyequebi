import { Component, OnInit } from '@angular/core';
import { ResumenGeneralChart } from 'src/app/model/chartResumenGeneralModel';
import { ResumenGeneralService } from 'src/app/service/resumen-general.service';

@Component({
  selector: 'app-ventas-periodo',
  templateUrl: './ventas-periodo.component.html',
  styleUrls: ['./ventas-periodo.component.scss'],
})
export class VentasPeriodoComponent implements OnInit {

  dateFrom: Date;
  dateEnd: Date;


  single: any[] = null;

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = "Fecha";
  showYAxisLabel = true;
  yAxisLabel = "Ganacia";

  colorScheme = {
    domain: ["#5AA454", "#A10A28", "#C7B42C", "#AAAAAA"]
  };

  constructor(private resumenGeneralService: ResumenGeneralService) {
    this.view = [innerWidth / 1.4, 400];
  }

  ngOnInit() {}

  public async loadData(event: Event) {
    if(this.dateEnd && this.dateFrom) {
      let dataGen = await this.resumenGeneralService.getVentasByTwoDates(new Date(this.dateFrom) , new Date(this.dateEnd))
      this.single = dataGen.map( data => {return { name: data.fecha, value: Number(data.total) }} );
    }
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 1.35, 500];
  }

}
