import { Component, OnInit } from '@angular/core';
import { Series, VentasPorUsuarioChart } from 'src/app/model/chartVentasPorUsuarioModel';
import { VentasPorUsuarioService } from 'src/app/service/ventas-por-usuario.service';
import { ventasPorUsuario } from 'src/assets/data/dataVentasPorUsuario';

@Component({
  selector: 'app-ventas-mesero',
  templateUrl: './ventas-mesero.component.html',
  styleUrls: ['./ventas-mesero.component.scss'],
})
export class VentasMeseroComponent implements OnInit {
  users: string[] = ['GERARDO SALAS','MARICELA RUIZ','Zuleima Ramirez','VERONICA  PRIETO','Mario Ramirez'];
  user: string;
  dataUser: any = null;

  view: any[] = [700, 300];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Fecha';
  yAxisLabel: string = 'Dinero';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };



  constructor(private ventasPorUsuarioService: VentasPorUsuarioService) {
    this.view = [innerWidth / 1.4, 400];
  }

  ngOnInit() {
  }

  public async loadData(event: Event) {
    let userInfo = await this.ventasPorUsuarioService.getVentasByName(this.user)
    let series: Series[] = await userInfo.map( ventas => {return {name:ventas.fecha,value:ventas.ventas };})
    let dataChart: VentasPorUsuarioChart = { name: userInfo[0].nombre,series: series }
    this.dataUser = [dataChart];
  }
  onResize(event) {
    this.view = [event.target.innerWidth / 1.35, 500];
  }
}
