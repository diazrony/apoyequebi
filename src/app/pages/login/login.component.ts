import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  user: string;
  password: string;
  constructor(private router: Router,
              public toastController: ToastController,
              public localStoreServ: LocalstorageService) { }

  ngOnInit() {}

  async login(event: Event) {
    event.preventDefault();
    if (this.password === '12345678') {
      this.localStoreServ.setUser(this.user);
      this.password = null;
      this.user = null;
      this.toastController.dismiss();
      this.router.navigate(['home']);
    }else {
      await this.presentToastWithOptions();
      setTimeout(() => {
        this.toastController.dismiss();
      }, 2000);
    }
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Mensaje',
      color: "danger",
      message: 'Contraseña Incorrecta',
      position: 'middle',
    });
    await toast.present();
  }
}
