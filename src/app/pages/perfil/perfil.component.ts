import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss'],
})
export class PerfilComponent implements OnInit {
  user: string;

  constructor(private localStoreServ: LocalstorageService) { }

  ngOnInit() {
    this.user = this.localStoreServ.getUser();
  }

}
