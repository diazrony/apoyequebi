import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  user: string;

  constructor(private localStoreServ: LocalstorageService) { }

  ngOnInit() {
    this.user = this.localStoreServ.getUser();
  }

}
