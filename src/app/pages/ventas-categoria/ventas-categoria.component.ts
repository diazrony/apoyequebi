import { Component, OnInit } from '@angular/core';
import { dataVCategoria } from 'src/assets/data/dataVCategoria';

@Component({
  selector: 'app-ventas-categoria',
  templateUrl: './ventas-categoria.component.html',
  styleUrls: ['./ventas-categoria.component.scss'],
})
export class VentasCategoriaComponent implements OnInit {

  view: any[] = [450, 300];
  data;

  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';

  colorScheme = {
    domain: ['#DAF7A6', '#FFC300', '#FF5733', '#C70039','#900C3F','#581845']
  };

  constructor() {
    this.view = [innerWidth / 1, 200];
  }

  ngOnInit() {
    this.data = dataVCategoria;
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 1.35, 500];
  }
}
