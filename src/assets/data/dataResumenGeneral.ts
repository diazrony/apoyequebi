export const dataResumenGeneral = [
      {
          "fecha": "01/02/2021",
          "operaciones": "10",
          "subtotal": "7113.79 ",
          "descuento": "299.14 ",
          "propina": "699.50 ",
          "impuestos": "1090.34 ",
          "total": "8604.50 "
      },
      {
          "fecha": "01/03/2020",
          "operaciones": "23",
          "subtotal": "27293.10 ",
          "descuento": "1175.86 ",
          "propina": "3165.15 ",
          "impuestos": "4178.76 ",
          "total": "33461.15 "
      },
      {
          "fecha": "01/03/2021",
          "operaciones": "19",
          "subtotal": "18250.86 ",
          "descuento": "0.00 ",
          "propina": "2811.00 ",
          "impuestos": "2920.14 ",
          "total": "23982.00 "
      },
      {
          "fecha": "01/04/2020",
          "operaciones": "7",
          "subtotal": "10085.34 ",
          "descuento": "47.41 ",
          "propina": "1317.00 ",
          "impuestos": "1606.07 ",
          "total": "12961.00 "
      },
      {
          "fecha": "01/05/2020",
          "operaciones": "6",
          "subtotal": "4295.69 ",
          "descuento": "68.97 ",
          "propina": "818.00 ",
          "impuestos": "676.28 ",
          "total": "5721.00 "
      },
      {
          "fecha": "01/06/2020",
          "operaciones": "9",
          "subtotal": "12347.41 ",
          "descuento": "109.48 ",
          "propina": "2547.00 ",
          "impuestos": "1958.07 ",
          "total": "16743.00 "
      },
      {
          "fecha": "01/07/2020",
          "operaciones": "8",
          "subtotal": "8252.59 ",
          "descuento": "0.00 ",
          "propina": "1727.00 ",
          "impuestos": "1320.41 ",
          "total": "11300.00 "
      },
      {
          "fecha": "01/08/2020",
          "operaciones": "22",
          "subtotal": "24042.24 ",
          "descuento": "710.34 ",
          "propina": "2889.00 ",
          "impuestos": "3733.10 ",
          "total": "29954.00 "
      },
      {
          "fecha": "01/09/2020",
          "operaciones": "14",
          "subtotal": "13333.62 ",
          "descuento": "455.17 ",
          "propina": "1656.05 ",
          "impuestos": "2060.55 ",
          "total": "16595.05 "
      },
      {
          "fecha": "01/10/2020",
          "operaciones": "17",
          "subtotal": "18134.48 ",
          "descuento": "0.00 ",
          "propina": "2147.00 ",
          "impuestos": "2901.52 ",
          "total": "23183.00 "
      },
      {
          "fecha": "01/11/2020",
          "operaciones": "13",
          "subtotal": "9837.07 ",
          "descuento": "103.45 ",
          "propina": "1336.00 ",
          "impuestos": "1557.38 ",
          "total": "12627.00 "
      },
      {
          "fecha": "01/12/2020",
          "operaciones": "16",
          "subtotal": "12365.52 ",
          "descuento": "0.00 ",
          "propina": "1547.00 ",
          "impuestos": "1978.48 ",
          "total": "15891.00 "
      },
      {
          "fecha": "02/01/2021",
          "operaciones": "8",
          "subtotal": "3775.86 ",
          "descuento": "0.00 ",
          "propina": "471.00 ",
          "impuestos": "604.14 ",
          "total": "4851.00 "
      },
      {
          "fecha": "02/02/2021",
          "operaciones": "15",
          "subtotal": "12057.76 ",
          "descuento": "715.52 ",
          "propina": "1212.00 ",
          "impuestos": "1814.76 ",
          "total": "14369.00 "
      },
      {
          "fecha": "02/03/2020",
          "operaciones": "12",
          "subtotal": "11185.34 ",
          "descuento": "475.00 ",
          "propina": "1540.00 ",
          "impuestos": "1713.66 ",
          "total": "13964.00 "
      },
      {
          "fecha": "02/04/2020",
          "operaciones": "3",
          "subtotal": "2230.17 ",
          "descuento": "0.00 ",
          "propina": "380.00 ",
          "impuestos": "356.83 ",
          "total": "2967.00 "
      },
      {
          "fecha": "02/05/2020",
          "operaciones": "5",
          "subtotal": "3868.10 ",
          "descuento": "0.00 ",
          "propina": "572.00 ",
          "impuestos": "618.90 ",
          "total": "5059.00 "
      },
      {
          "fecha": "02/06/2020",
          "operaciones": "6",
          "subtotal": "2618.97 ",
          "descuento": "115.34 ",
          "propina": "448.00 ",
          "impuestos": "400.58 ",
          "total": "3352.20 "
      },
      {
          "fecha": "02/07/2020",
          "operaciones": "5",
          "subtotal": "2799.14 ",
          "descuento": "40.09 ",
          "propina": "369.50 ",
          "impuestos": "441.45 ",
          "total": "3570.00 "
      },
      {
          "fecha": "02/08/2020",
          "operaciones": "13",
          "subtotal": "12514.65 ",
          "descuento": "73.28 ",
          "propina": "1334.00 ",
          "impuestos": "1990.62 ",
          "total": "15765.99 "
      },
      {
          "fecha": "02/09/2020",
          "operaciones": "16",
          "subtotal": "18551.72 ",
          "descuento": "0.00 ",
          "propina": "2304.00 ",
          "impuestos": "2968.28 ",
          "total": "23824.00 "
      },
      {
          "fecha": "02/10/2020",
          "operaciones": "32",
          "subtotal": "36390.52 ",
          "descuento": "1733.63 ",
          "propina": "3755.00 ",
          "impuestos": "5545.10 ",
          "total": "43956.99 "
      },
      {
          "fecha": "02/11/2020",
          "operaciones": "14",
          "subtotal": "14150.86 ",
          "descuento": "0.00 ",
          "propina": "1862.00 ",
          "impuestos": "2264.14 ",
          "total": "18277.00 "
      },
      {
          "fecha": "02/12/2020",
          "operaciones": "19",
          "subtotal": "23848.28 ",
          "descuento": "590.52 ",
          "propina": "2725.00 ",
          "impuestos": "3721.24 ",
          "total": "29704.00 "
      },
      {
          "fecha": "03/01/2021",
          "operaciones": "10",
          "subtotal": "9686.21 ",
          "descuento": "754.31 ",
          "propina": "997.00 ",
          "impuestos": "1429.10 ",
          "total": "11358.00 "
      },
      {
          "fecha": "03/02/2021",
          "operaciones": "8",
          "subtotal": "7034.48 ",
          "descuento": "81.90 ",
          "propina": "1058.00 ",
          "impuestos": "1112.41 ",
          "total": "9123.00 "
      },
      {
          "fecha": "03/03/2020",
          "operaciones": "32",
          "subtotal": "33875.00 ",
          "descuento": "385.34 ",
          "propina": "4859.55 ",
          "impuestos": "5358.34 ",
          "total": "43707.55 "
      },
      {
          "fecha": "03/04/2020",
          "operaciones": "4",
          "subtotal": "2504.31 ",
          "descuento": "0.00 ",
          "propina": "394.00 ",
          "impuestos": "400.69 ",
          "total": "3299.00 "
      },
      {
          "fecha": "03/05/2020",
          "operaciones": "5",
          "subtotal": "2883.62 ",
          "descuento": "152.85 ",
          "propina": "773.50 ",
          "impuestos": "436.92 ",
          "total": "3941.19 "
      },
      {
          "fecha": "03/06/2020",
          "operaciones": "10",
          "subtotal": "9691.38 ",
          "descuento": "162.07 ",
          "propina": "1258.75 ",
          "impuestos": "1524.69 ",
          "total": "12312.75 "
      },
      {
          "fecha": "03/07/2020",
          "operaciones": "15",
          "subtotal": "17174.14 ",
          "descuento": "71.55 ",
          "propina": "3007.00 ",
          "impuestos": "2736.41 ",
          "total": "22846.00 "
      },
      {
          "fecha": "03/08/2020",
          "operaciones": "10",
          "subtotal": "5572.41 ",
          "descuento": "422.41 ",
          "propina": "650.00 ",
          "impuestos": "824.00 ",
          "total": "6624.00 "
      },
      {
          "fecha": "03/09/2020",
          "operaciones": "16",
          "subtotal": "11761.21 ",
          "descuento": "275.86 ",
          "propina": "1475.00 ",
          "impuestos": "1837.66 ",
          "total": "14798.00 "
      },
      {
          "fecha": "03/10/2020",
          "operaciones": "29",
          "subtotal": "29297.41 ",
          "descuento": "254.31 ",
          "propina": "3926.00 ",
          "impuestos": "4646.90 ",
          "total": "37616.00 "
      },
      {
          "fecha": "03/11/2020",
          "operaciones": "16",
          "subtotal": "16348.27 ",
          "descuento": "456.04 ",
          "propina": "2023.00 ",
          "impuestos": "2542.76 ",
          "total": "20457.99 "
      },
      {
          "fecha": "03/12/2020",
          "operaciones": "18",
          "subtotal": "23421.55 ",
          "descuento": "232.76 ",
          "propina": "2700.00 ",
          "impuestos": "3710.21 ",
          "total": "29599.00 "
      },
      {
          "fecha": "04/01/2021",
          "operaciones": "7",
          "subtotal": "7674.14 ",
          "descuento": "473.28 ",
          "propina": "700.00 ",
          "impuestos": "1152.14 ",
          "total": "9053.00 "
      },
      {
          "fecha": "04/02/2021",
          "operaciones": "12",
          "subtotal": "12061.21 ",
          "descuento": "0.00 ",
          "propina": "1550.00 ",
          "impuestos": "1929.79 ",
          "total": "15541.00 "
      },
      {
          "fecha": "04/03/2020",
          "operaciones": "23",
          "subtotal": "19001.72 ",
          "descuento": "0.00 ",
          "propina": "2505.70 ",
          "impuestos": "3040.28 ",
          "total": "24547.70 "
      },
      {
          "fecha": "04/04/2020",
          "operaciones": "3",
          "subtotal": "1388.79 ",
          "descuento": "0.00 ",
          "propina": "211.00 ",
          "impuestos": "222.21 ",
          "total": "1822.00 "
      },
      {
          "fecha": "04/05/2020",
          "operaciones": "4",
          "subtotal": "2233.62 ",
          "descuento": "386.63 ",
          "propina": "119.00 ",
          "impuestos": "295.52 ",
          "total": "2261.51 "
      },
      {
          "fecha": "04/06/2020",
          "operaciones": "3",
          "subtotal": "2881.90 ",
          "descuento": "123.71 ",
          "propina": "358.50 ",
          "impuestos": "441.31 ",
          "total": "3558.00 "
      },
      {
          "fecha": "04/07/2020",
          "operaciones": "15",
          "subtotal": "12390.52 ",
          "descuento": "0.00 ",
          "propina": "974.50 ",
          "impuestos": "1982.48 ",
          "total": "15347.50 "
      },
      {
          "fecha": "04/08/2020",
          "operaciones": "11",
          "subtotal": "12243.97 ",
          "descuento": "189.66 ",
          "propina": "1510.52 ",
          "impuestos": "1928.69 ",
          "total": "15493.52 "
      },
      {
          "fecha": "04/09/2020",
          "operaciones": "32",
          "subtotal": "33175.00 ",
          "descuento": "740.52 ",
          "propina": "4896.00 ",
          "impuestos": "5189.52 ",
          "total": "42520.00 "
      },
      {
          "fecha": "04/10/2020",
          "operaciones": "21",
          "subtotal": "12631.03 ",
          "descuento": "0.00 ",
          "propina": "1332.00 ",
          "impuestos": "2020.97 ",
          "total": "15984.00 "
      },
      {
          "fecha": "04/11/2020",
          "operaciones": "17",
          "subtotal": "17456.90 ",
          "descuento": "0.00 ",
          "propina": "2481.85 ",
          "impuestos": "2793.10 ",
          "total": "22731.85 "
      },
      {
          "fecha": "04/12/2020",
          "operaciones": "33",
          "subtotal": "32503.45 ",
          "descuento": "0.00 ",
          "propina": "4805.80 ",
          "impuestos": "5200.55 ",
          "total": "42509.80 "
      },
      {
          "fecha": "05/02/2021",
          "operaciones": "21",
          "subtotal": "21024.14 ",
          "descuento": "0.00 ",
          "propina": "3012.65 ",
          "impuestos": "3363.86 ",
          "total": "27400.65 "
      },
      {
          "fecha": "05/03/2020",
          "operaciones": "19",
          "subtotal": "28006.03 ",
          "descuento": "2067.24 ",
          "propina": "3557.00 ",
          "impuestos": "4150.21 ",
          "total": "33646.00 "
      },
      {
          "fecha": "05/04/2020",
          "operaciones": "8",
          "subtotal": "3335.34 ",
          "descuento": "0.00 ",
          "propina": "444.00 ",
          "impuestos": "533.66 ",
          "total": "4313.00 "
      },
      {
          "fecha": "05/05/2020",
          "operaciones": "1",
          "subtotal": "129.31 ",
          "descuento": "0.00 ",
          "propina": "15.00 ",
          "impuestos": "20.69 ",
          "total": "165.00 "
      },
      {
          "fecha": "05/06/2020",
          "operaciones": "9",
          "subtotal": "6591.38 ",
          "descuento": "435.78 ",
          "propina": "800.00 ",
          "impuestos": "984.90 ",
          "total": "7940.50 "
      },
      {
          "fecha": "05/07/2020",
          "operaciones": "8",
          "subtotal": "4213.79 ",
          "descuento": "0.00 ",
          "propina": "573.00 ",
          "impuestos": "674.21 ",
          "total": "5461.00 "
      },
      {
          "fecha": "05/08/2020",
          "operaciones": "15",
          "subtotal": "19028.45 ",
          "descuento": "453.45 ",
          "propina": "2529.00 ",
          "impuestos": "2972.00 ",
          "total": "24076.00 "
      },
      {
          "fecha": "05/09/2020",
          "operaciones": "28",
          "subtotal": "27038.79 ",
          "descuento": "71.55 ",
          "propina": "3083.60 ",
          "impuestos": "4314.76 ",
          "total": "34365.60 "
      },
      {
          "fecha": "05/10/2020",
          "operaciones": "18",
          "subtotal": "12047.41 ",
          "descuento": "944.83 ",
          "propina": "1486.95 ",
          "impuestos": "1776.41 ",
          "total": "14365.95 "
      },
      {
          "fecha": "05/11/2020",
          "operaciones": "20",
          "subtotal": "26990.52 ",
          "descuento": "148.28 ",
          "propina": "3134.00 ",
          "impuestos": "4294.76 ",
          "total": "34270.99 "
      },
      {
          "fecha": "05/12/2020",
          "operaciones": "27",
          "subtotal": "39710.34 ",
          "descuento": "2337.92 ",
          "propina": "4492.00 ",
          "impuestos": "5979.59 ",
          "total": "47844.01 "
      },
      {
          "fecha": "06/01/2021",
          "operaciones": "4",
          "subtotal": "4825.86 ",
          "descuento": "0.00 ",
          "propina": "730.00 ",
          "impuestos": "772.14 ",
          "total": "6328.00 "
      },
      {
          "fecha": "06/02/2021",
          "operaciones": "15",
          "subtotal": "22106.03 ",
          "descuento": "0.00 ",
          "propina": "3076.40 ",
          "impuestos": "3536.97 ",
          "total": "28719.40 "
      },
      {
          "fecha": "06/03/2020",
          "operaciones": "28",
          "subtotal": "39287.07 ",
          "descuento": "0.00 ",
          "propina": "5491.35 ",
          "impuestos": "6285.93 ",
          "total": "51064.35 "
      },
      {
          "fecha": "06/04/2020",
          "operaciones": "6",
          "subtotal": "4215.52 ",
          "descuento": "0.00 ",
          "propina": "521.00 ",
          "impuestos": "674.48 ",
          "total": "5411.00 "
      },
      {
          "fecha": "06/05/2020",
          "operaciones": "2",
          "subtotal": "439.66 ",
          "descuento": "22.41 ",
          "propina": "50.00 ",
          "impuestos": "66.76 ",
          "total": "534.00 "
      },
      {
          "fecha": "06/06/2020",
          "operaciones": "4",
          "subtotal": "1910.34 ",
          "descuento": "0.00 ",
          "propina": "190.00 ",
          "impuestos": "305.66 ",
          "total": "2406.00 "
      },
      {
          "fecha": "06/07/2020",
          "operaciones": "6",
          "subtotal": "7797.41 ",
          "descuento": "0.00 ",
          "propina": "882.00 ",
          "impuestos": "1247.59 ",
          "total": "9927.00 "
      },
      {
          "fecha": "06/08/2020",
          "operaciones": "13",
          "subtotal": "12095.69 ",
          "descuento": "0.00 ",
          "propina": "1930.00 ",
          "impuestos": "1935.31 ",
          "total": "15961.00 "
      },
      {
          "fecha": "06/09/2020",
          "operaciones": "18",
          "subtotal": "17114.65 ",
          "descuento": "0.00 ",
          "propina": "1789.00 ",
          "impuestos": "2738.34 ",
          "total": "21642.00 "
      },
      {
          "fecha": "06/10/2020",
          "operaciones": "16",
          "subtotal": "11332.76 ",
          "descuento": "161.21 ",
          "propina": "1756.00 ",
          "impuestos": "1787.45 ",
          "total": "14715.00 "
      },
      {
          "fecha": "06/11/2020",
          "operaciones": "28",
          "subtotal": "32638.79 ",
          "descuento": "0.00 ",
          "propina": "3898.70 ",
          "impuestos": "5222.21 ",
          "total": "41759.70 "
      },
      {
          "fecha": "06/12/2020",
          "operaciones": "10",
          "subtotal": "9922.41 ",
          "descuento": "537.93 ",
          "propina": "1094.00 ",
          "impuestos": "1501.52 ",
          "total": "11980.00 "
      },
      {
          "fecha": "07/01/2021",
          "operaciones": "7",
          "subtotal": "5088.79 ",
          "descuento": "0.00 ",
          "propina": "919.00 ",
          "impuestos": "814.21 ",
          "total": "6822.00 "
      },
      {
          "fecha": "07/02/2021",
          "operaciones": "11",
          "subtotal": "10243.10 ",
          "descuento": "326.72 ",
          "propina": "1199.00 ",
          "impuestos": "1586.62 ",
          "total": "12702.00 "
      },
      {
          "fecha": "07/03/2020",
          "operaciones": "33",
          "subtotal": "30008.62 ",
          "descuento": "0.00 ",
          "propina": "3488.48 ",
          "impuestos": "4801.38 ",
          "total": "38298.48 "
      },
      {
          "fecha": "07/04/2020",
          "operaciones": "4",
          "subtotal": "3553.45 ",
          "descuento": "0.00 ",
          "propina": "562.00 ",
          "impuestos": "568.55 ",
          "total": "4684.00 "
      },
      {
          "fecha": "07/05/2020",
          "operaciones": "4",
          "subtotal": "3643.97 ",
          "descuento": "0.00 ",
          "propina": "733.10 ",
          "impuestos": "583.03 ",
          "total": "4960.10 "
      },
      {
          "fecha": "07/06/2020",
          "operaciones": "8",
          "subtotal": "5923.28 ",
          "descuento": "0.00 ",
          "propina": "699.00 ",
          "impuestos": "947.72 ",
          "total": "7570.00 "
      },
      {
          "fecha": "07/07/2020",
          "operaciones": "5",
          "subtotal": "3549.14 ",
          "descuento": "0.00 ",
          "propina": "568.00 ",
          "impuestos": "567.86 ",
          "total": "4685.00 "
      },
      {
          "fecha": "07/08/2020",
          "operaciones": "22",
          "subtotal": "23375.86 ",
          "descuento": "318.10 ",
          "propina": "3252.00 ",
          "impuestos": "3689.24 ",
          "total": "29999.00 "
      },
      {
          "fecha": "07/09/2020",
          "operaciones": "16",
          "subtotal": "11314.65 ",
          "descuento": "1045.69 ",
          "propina": "1485.70 ",
          "impuestos": "1643.03 ",
          "total": "13397.70 "
      },
      {
          "fecha": "07/10/2020",
          "operaciones": "16",
          "subtotal": "15037.93 ",
          "descuento": "0.00 ",
          "propina": "2108.50 ",
          "impuestos": "2406.07 ",
          "total": "19552.50 "
      },
      {
          "fecha": "07/11/2020",
          "operaciones": "21",
          "subtotal": "22987.07 ",
          "descuento": "2367.24 ",
          "propina": "2404.30 ",
          "impuestos": "3299.17 ",
          "total": "26323.30 "
      },
      {
          "fecha": "07/12/2020",
          "operaciones": "17",
          "subtotal": "16398.28 ",
          "descuento": "0.00 ",
          "propina": "2057.00 ",
          "impuestos": "2623.72 ",
          "total": "21079.00 "
      },
      {
          "fecha": "08/01/2021",
          "operaciones": "14",
          "subtotal": "10845.69 ",
          "descuento": "0.00 ",
          "propina": "1581.00 ",
          "impuestos": "1735.31 ",
          "total": "14162.00 "
      },
      {
          "fecha": "08/02/2021",
          "operaciones": "9",
          "subtotal": "9075.00 ",
          "descuento": "0.00 ",
          "propina": "1355.00 ",
          "impuestos": "1452.00 ",
          "total": "11882.00 "
      },
      {
          "fecha": "08/03/2020",
          "operaciones": "46",
          "subtotal": "58216.38 ",
          "descuento": "6309.49 ",
          "propina": "6506.10 ",
          "impuestos": "8305.10 ",
          "total": "66718.09 "
      },
      {
          "fecha": "08/04/2020",
          "operaciones": "3",
          "subtotal": "1756.90 ",
          "descuento": "0.00 ",
          "propina": "215.00 ",
          "impuestos": "281.10 ",
          "total": "2253.00 "
      },
      {
          "fecha": "08/05/2020",
          "operaciones": "5",
          "subtotal": "7937.93 ",
          "descuento": "261.98 ",
          "propina": "837.90 ",
          "impuestos": "1228.15 ",
          "total": "9742.00 "
      },
      {
          "fecha": "08/06/2020",
          "operaciones": "2",
          "subtotal": "2639.66 ",
          "descuento": "0.00 ",
          "propina": "329.00 ",
          "impuestos": "422.34 ",
          "total": "3391.00 "
      },
      {
          "fecha": "08/07/2020",
          "operaciones": "11",
          "subtotal": "9368.10 ",
          "descuento": "2567.24 ",
          "propina": "925.00 ",
          "impuestos": "1088.14 ",
          "total": "8814.00 "
      },
      {
          "fecha": "08/08/2020",
          "operaciones": "22",
          "subtotal": "22624.14 ",
          "descuento": "117.24 ",
          "propina": "2555.00 ",
          "impuestos": "3601.10 ",
          "total": "28663.00 "
      },
      {
          "fecha": "08/09/2020",
          "operaciones": "17",
          "subtotal": "13488.79 ",
          "descuento": "366.38 ",
          "propina": "1805.55 ",
          "impuestos": "2099.59 ",
          "total": "17027.55 "
      },
      {
          "fecha": "08/10/2020",
          "operaciones": "18",
          "subtotal": "16069.83 ",
          "descuento": "0.00 ",
          "propina": "1623.00 ",
          "impuestos": "2571.17 ",
          "total": "20264.00 "
      },
      {
          "fecha": "08/11/2020",
          "operaciones": "14",
          "subtotal": "14454.31 ",
          "descuento": "0.00 ",
          "propina": "1998.00 ",
          "impuestos": "2312.69 ",
          "total": "18765.00 "
      },
      {
          "fecha": "08/12/2020",
          "operaciones": "18",
          "subtotal": "11406.03 ",
          "descuento": "0.00 ",
          "propina": "1255.00 ",
          "impuestos": "1824.97 ",
          "total": "14486.00 "
      },
      {
          "fecha": "09/01/2021",
          "operaciones": "12",
          "subtotal": "10618.96 ",
          "descuento": "0.00 ",
          "propina": "1722.00 ",
          "impuestos": "1699.03 ",
          "total": "14040.00 "
      },
      {
          "fecha": "09/02/2021",
          "operaciones": "13",
          "subtotal": "7930.17 ",
          "descuento": "253.45 ",
          "propina": "1103.00 ",
          "impuestos": "1228.28 ",
          "total": "10008.00 "
      },
      {
          "fecha": "09/03/2020",
          "operaciones": "29",
          "subtotal": "33649.14 ",
          "descuento": "523.28 ",
          "propina": "4067.25 ",
          "impuestos": "5300.14 ",
          "total": "42493.24 "
      },
      {
          "fecha": "09/04/2020",
          "operaciones": "4",
          "subtotal": "2612.07 ",
          "descuento": "0.00 ",
          "propina": "461.00 ",
          "impuestos": "417.93 ",
          "total": "3491.00 "
      },
      {
          "fecha": "09/05/2020",
          "operaciones": "16",
          "subtotal": "16656.90 ",
          "descuento": "190.09 ",
          "propina": "1908.00 ",
          "impuestos": "2634.69 ",
          "total": "21009.50 "
      },
      {
          "fecha": "09/06/2020",
          "operaciones": "5",
          "subtotal": "5322.41 ",
          "descuento": "0.00 ",
          "propina": "739.00 ",
          "impuestos": "851.59 ",
          "total": "6913.00 "
      },
      {
          "fecha": "09/07/2020",
          "operaciones": "3",
          "subtotal": "3281.03 ",
          "descuento": "0.00 ",
          "propina": "383.00 ",
          "impuestos": "524.97 ",
          "total": "4189.00 "
      },
      {
          "fecha": "09/08/2020",
          "operaciones": "11",
          "subtotal": "15349.14 ",
          "descuento": "526.72 ",
          "propina": "1867.00 ",
          "impuestos": "2371.59 ",
          "total": "19061.00 "
      },
      {
          "fecha": "09/09/2020",
          "operaciones": "7",
          "subtotal": "10285.34 ",
          "descuento": "0.00 ",
          "propina": "1279.00 ",
          "impuestos": "1645.66 ",
          "total": "13210.00 "
      },
      {
          "fecha": "09/10/2020",
          "operaciones": "14",
          "subtotal": "18260.34 ",
          "descuento": "0.00 ",
          "propina": "2210.85 ",
          "impuestos": "2921.66 ",
          "total": "23392.85 "
      },
      {
          "fecha": "09/11/2020",
          "operaciones": "12",
          "subtotal": "13960.34 ",
          "descuento": "554.31 ",
          "propina": "2003.00 ",
          "impuestos": "2144.97 ",
          "total": "17554.00 "
      },
      {
          "fecha": "09/12/2020",
          "operaciones": "14",
          "subtotal": "8197.41 ",
          "descuento": "202.59 ",
          "propina": "1167.00 ",
          "impuestos": "1279.17 ",
          "total": "10441.00 "
      },
      {
          "fecha": "10/01/2021",
          "operaciones": "11",
          "subtotal": "10260.34 ",
          "descuento": "0.00 ",
          "propina": "690.00 ",
          "impuestos": "1641.66 ",
          "total": "12592.00 "
      },
      {
          "fecha": "10/02/2021",
          "operaciones": "13",
          "subtotal": "14287.93 ",
          "descuento": "47.41 ",
          "propina": "2297.00 ",
          "impuestos": "2278.48 ",
          "total": "18816.00 "
      },
      {
          "fecha": "10/03/2020",
          "operaciones": "24",
          "subtotal": "26551.72 ",
          "descuento": "1138.79 ",
          "propina": "3388.90 ",
          "impuestos": "4066.07 ",
          "total": "32867.90 "
      },
      {
          "fecha": "10/04/2020",
          "operaciones": "6",
          "subtotal": "2299.14 ",
          "descuento": "129.31 ",
          "propina": "235.00 ",
          "impuestos": "347.17 ",
          "total": "2752.00 "
      },
      {
          "fecha": "10/05/2020",
          "operaciones": "61",
          "subtotal": "71141.81 ",
          "descuento": "310.78 ",
          "propina": "3432.85 ",
          "impuestos": "11332.97 ",
          "total": "85596.85 "
      },
      {
          "fecha": "10/06/2020",
          "operaciones": "4",
          "subtotal": "3422.41 ",
          "descuento": "19.40 ",
          "propina": "727.50 ",
          "impuestos": "544.48 ",
          "total": "4675.00 "
      },
      {
          "fecha": "10/07/2020",
          "operaciones": "24",
          "subtotal": "20201.72 ",
          "descuento": "0.00 ",
          "propina": "2389.00 ",
          "impuestos": "3232.28 ",
          "total": "25823.00 "
      },
      {
          "fecha": "10/08/2020",
          "operaciones": "8",
          "subtotal": "6145.69 ",
          "descuento": "1075.86 ",
          "propina": "714.00 ",
          "impuestos": "811.17 ",
          "total": "6595.00 "
      },
      {
          "fecha": "10/09/2020",
          "operaciones": "19",
          "subtotal": "18790.52 ",
          "descuento": "0.00 ",
          "propina": "2349.00 ",
          "impuestos": "3006.48 ",
          "total": "24146.00 "
      },
      {
          "fecha": "10/10/2020",
          "operaciones": "29",
          "subtotal": "45359.48 ",
          "descuento": "801.72 ",
          "propina": "5526.00 ",
          "impuestos": "7129.24 ",
          "total": "57213.00 "
      },
      {
          "fecha": "10/11/2020",
          "operaciones": "12",
          "subtotal": "12561.21 ",
          "descuento": "82.76 ",
          "propina": "1603.00 ",
          "impuestos": "1996.55 ",
          "total": "16078.00 "
      },
      {
          "fecha": "10/12/2020",
          "operaciones": "12",
          "subtotal": "10525.86 ",
          "descuento": "0.00 ",
          "propina": "1182.85 ",
          "impuestos": "1684.14 ",
          "total": "13392.85 "
      },
      {
          "fecha": "11/01/2021",
          "operaciones": "10",
          "subtotal": "7656.03 ",
          "descuento": "0.00 ",
          "propina": "1255.00 ",
          "impuestos": "1224.97 ",
          "total": "10136.00 "
      },
      {
          "fecha": "11/02/2021",
          "operaciones": "6",
          "subtotal": "4176.72 ",
          "descuento": "0.00 ",
          "propina": "637.00 ",
          "impuestos": "668.28 ",
          "total": "5482.00 "
      },
      {
          "fecha": "11/03/2020",
          "operaciones": "19",
          "subtotal": "18713.79 ",
          "descuento": "0.00 ",
          "propina": "2515.40 ",
          "impuestos": "2994.21 ",
          "total": "24223.40 "
      },
      {
          "fecha": "11/04/2020",
          "operaciones": "6",
          "subtotal": "4187.93 ",
          "descuento": "0.00 ",
          "propina": "388.00 ",
          "impuestos": "670.07 ",
          "total": "5246.00 "
      },
      {
          "fecha": "11/05/2020",
          "operaciones": "3",
          "subtotal": "4981.03 ",
          "descuento": "0.00 ",
          "propina": "1082.00 ",
          "impuestos": "796.97 ",
          "total": "6860.00 "
      },
      {
          "fecha": "11/06/2020",
          "operaciones": "15",
          "subtotal": "7831.90 ",
          "descuento": "64.66 ",
          "propina": "778.00 ",
          "impuestos": "1242.76 ",
          "total": "9787.99 "
      },
      {
          "fecha": "11/07/2020",
          "operaciones": "11",
          "subtotal": "9655.17 ",
          "descuento": "0.00 ",
          "propina": "1259.00 ",
          "impuestos": "1544.83 ",
          "total": "12459.00 "
      },
      {
          "fecha": "11/08/2020",
          "operaciones": "15",
          "subtotal": "9177.59 ",
          "descuento": "0.00 ",
          "propina": "1201.70 ",
          "impuestos": "1468.41 ",
          "total": "11847.70 "
      },
      {
          "fecha": "11/09/2020",
          "operaciones": "22",
          "subtotal": "18494.83 ",
          "descuento": "0.00 ",
          "propina": "2819.00 ",
          "impuestos": "2959.17 ",
          "total": "24273.00 "
      },
      {
          "fecha": "11/10/2020",
          "operaciones": "10",
          "subtotal": "9830.17 ",
          "descuento": "266.38 ",
          "propina": "1008.00 ",
          "impuestos": "1530.21 ",
          "total": "12102.00 "
      },
      {
          "fecha": "11/11/2020",
          "operaciones": "10",
          "subtotal": "10395.69 ",
          "descuento": "0.00 ",
          "propina": "1158.00 ",
          "impuestos": "1663.31 ",
          "total": "13217.00 "
      },
      {
          "fecha": "11/12/2020",
          "operaciones": "38",
          "subtotal": "45959.48 ",
          "descuento": "212.07 ",
          "propina": "6007.00 ",
          "impuestos": "7319.59 ",
          "total": "59074.00 "
      },
      {
          "fecha": "12/01/2021",
          "operaciones": "6",
          "subtotal": "6000.86 ",
          "descuento": "0.00 ",
          "propina": "751.00 ",
          "impuestos": "960.14 ",
          "total": "7712.00 "
      },
      {
          "fecha": "12/02/2021",
          "operaciones": "14",
          "subtotal": "19200.86 ",
          "descuento": "256.90 ",
          "propina": "2410.20 ",
          "impuestos": "3031.03 ",
          "total": "24385.20 "
      },
      {
          "fecha": "12/03/2020",
          "operaciones": "36",
          "subtotal": "30256.03 ",
          "descuento": "823.28 ",
          "propina": "3592.00 ",
          "impuestos": "4709.24 ",
          "total": "37733.99 "
      },
      {
          "fecha": "12/04/2020",
          "operaciones": "8",
          "subtotal": "5209.48 ",
          "descuento": "197.84 ",
          "propina": "446.00 ",
          "impuestos": "801.86 ",
          "total": "6259.50 "
      },
      {
          "fecha": "12/05/2020",
          "operaciones": "10",
          "subtotal": "14578.45 ",
          "descuento": "280.60 ",
          "propina": "1978.50 ",
          "impuestos": "2287.66 ",
          "total": "18564.00 "
      },
      {
          "fecha": "12/06/2020",
          "operaciones": "15",
          "subtotal": "17036.21 ",
          "descuento": "146.98 ",
          "propina": "2463.00 ",
          "impuestos": "2702.28 ",
          "total": "22054.50 "
      },
      {
          "fecha": "12/07/2020",
          "operaciones": "10",
          "subtotal": "11018.10 ",
          "descuento": "581.90 ",
          "propina": "1215.00 ",
          "impuestos": "1669.79 ",
          "total": "13321.00 "
      },
      {
          "fecha": "12/08/2020",
          "operaciones": "22",
          "subtotal": "18525.86 ",
          "descuento": "426.72 ",
          "propina": "2368.95 ",
          "impuestos": "2895.86 ",
          "total": "23363.95 "
      },
      {
          "fecha": "12/09/2020",
          "operaciones": "35",
          "subtotal": "44082.76 ",
          "descuento": "512.07 ",
          "propina": "5044.00 ",
          "impuestos": "6971.31 ",
          "total": "55586.00 "
      },
      {
          "fecha": "12/10/2020",
          "operaciones": "11",
          "subtotal": "8937.07 ",
          "descuento": "0.00 ",
          "propina": "1169.00 ",
          "impuestos": "1429.93 ",
          "total": "11536.00 "
      },
      {
          "fecha": "12/11/2020",
          "operaciones": "21",
          "subtotal": "14670.69 ",
          "descuento": "0.00 ",
          "propina": "1635.00 ",
          "impuestos": "2347.31 ",
          "total": "18653.00 "
      },
      {
          "fecha": "12/12/2020",
          "operaciones": "15",
          "subtotal": "18472.41 ",
          "descuento": "0.00 ",
          "propina": "2108.00 ",
          "impuestos": "2955.59 ",
          "total": "23536.00 "
      },
      {
          "fecha": "13/01/2021",
          "operaciones": "9",
          "subtotal": "11312.07 ",
          "descuento": "0.00 ",
          "propina": "2170.00 ",
          "impuestos": "1809.93 ",
          "total": "15292.00 "
      },
      {
          "fecha": "13/02/2021",
          "operaciones": "46",
          "subtotal": "53247.41 ",
          "descuento": "0.00 ",
          "propina": "6463.05 ",
          "impuestos": "8519.59 ",
          "total": "68230.05 "
      },
      {
          "fecha": "13/03/2020",
          "operaciones": "26",
          "subtotal": "32887.93 ",
          "descuento": "129.31 ",
          "propina": "4190.50 ",
          "impuestos": "5241.38 ",
          "total": "42190.50 "
      },
      {
          "fecha": "13/04/2020",
          "operaciones": "4",
          "subtotal": "6409.48 ",
          "descuento": "0.00 ",
          "propina": "1029.00 ",
          "impuestos": "1025.52 ",
          "total": "8464.00 "
      },
      {
          "fecha": "13/05/2020",
          "operaciones": "4",
          "subtotal": "6411.21 ",
          "descuento": "0.00 ",
          "propina": "1284.00 ",
          "impuestos": "1025.79 ",
          "total": "8721.00 "
      },
      {
          "fecha": "13/06/2020",
          "operaciones": "10",
          "subtotal": "9357.76 ",
          "descuento": "485.78 ",
          "propina": "1350.00 ",
          "impuestos": "1419.52 ",
          "total": "11641.50 "
      },
      {
          "fecha": "13/07/2020",
          "operaciones": "9",
          "subtotal": "9321.55 ",
          "descuento": "0.00 ",
          "propina": "1219.00 ",
          "impuestos": "1491.45 ",
          "total": "12032.00 "
      },
      {
          "fecha": "13/08/2020",
          "operaciones": "10",
          "subtotal": "10811.21 ",
          "descuento": "0.00 ",
          "propina": "1311.45 ",
          "impuestos": "1729.79 ",
          "total": "13852.45 "
      },
      {
          "fecha": "13/09/2020",
          "operaciones": "11",
          "subtotal": "9289.65 ",
          "descuento": "255.17 ",
          "propina": "1164.90 ",
          "impuestos": "1445.52 ",
          "total": "11644.90 "
      },
      {
          "fecha": "13/10/2020",
          "operaciones": "7",
          "subtotal": "9649.14 ",
          "descuento": "64.66 ",
          "propina": "924.45 ",
          "impuestos": "1533.52 ",
          "total": "12042.44 "
      },
      {
          "fecha": "13/11/2020",
          "operaciones": "27",
          "subtotal": "33687.93 ",
          "descuento": "0.00 ",
          "propina": "4714.70 ",
          "impuestos": "5390.07 ",
          "total": "43792.70 "
      },
      {
          "fecha": "13/12/2020",
          "operaciones": "20",
          "subtotal": "22026.72 ",
          "descuento": "402.59 ",
          "propina": "2456.00 ",
          "impuestos": "3459.86 ",
          "total": "27540.00 "
      },
      {
          "fecha": "14/01/2021",
          "operaciones": "11",
          "subtotal": "9630.17 ",
          "descuento": "645.69 ",
          "propina": "1546.00 ",
          "impuestos": "1437.52 ",
          "total": "11968.00 "
      },
      {
          "fecha": "14/02/2021",
          "operaciones": "55",
          "subtotal": "59829.31 ",
          "descuento": "0.00 ",
          "propina": "6987.00 ",
          "impuestos": "9572.69 ",
          "total": "76389.00 "
      },
      {
          "fecha": "14/03/2020",
          "operaciones": "15",
          "subtotal": "14984.48 ",
          "descuento": "461.21 ",
          "propina": "1981.50 ",
          "impuestos": "2323.72 ",
          "total": "18828.50 "
      },
      {
          "fecha": "14/04/2020",
          "operaciones": "5",
          "subtotal": "6749.14 ",
          "descuento": "0.00 ",
          "propina": "1957.00 ",
          "impuestos": "1079.86 ",
          "total": "9786.00 "
      },
      {
          "fecha": "14/05/2020",
          "operaciones": "3",
          "subtotal": "4119.83 ",
          "descuento": "1027.59 ",
          "propina": "413.00 ",
          "impuestos": "494.76 ",
          "total": "4000.00 "
      },
      {
          "fecha": "14/06/2020",
          "operaciones": "8",
          "subtotal": "6932.76 ",
          "descuento": "139.06 ",
          "propina": "1023.80 ",
          "impuestos": "1086.99 ",
          "total": "8904.49 "
      },
      {
          "fecha": "14/07/2020",
          "operaciones": "13",
          "subtotal": "13757.76 ",
          "descuento": "1790.52 ",
          "propina": "1708.00 ",
          "impuestos": "1914.76 ",
          "total": "15590.00 "
      },
      {
          "fecha": "14/08/2020",
          "operaciones": "13",
          "subtotal": "19618.10 ",
          "descuento": "0.00 ",
          "propina": "2473.90 ",
          "impuestos": "3138.90 ",
          "total": "25230.90 "
      },
      {
          "fecha": "14/09/2020",
          "operaciones": "12",
          "subtotal": "10625.00 ",
          "descuento": "358.62 ",
          "propina": "1261.00 ",
          "impuestos": "1642.62 ",
          "total": "13170.00 "
      },
      {
          "fecha": "14/10/2020",
          "operaciones": "12",
          "subtotal": "10118.10 ",
          "descuento": "592.24 ",
          "propina": "1542.00 ",
          "impuestos": "1524.14 ",
          "total": "12592.00 "
      },
      {
          "fecha": "14/11/2020",
          "operaciones": "24",
          "subtotal": "25448.28 ",
          "descuento": "206.90 ",
          "propina": "3298.00 ",
          "impuestos": "4038.62 ",
          "total": "32578.00 "
      },
      {
          "fecha": "14/12/2020",
          "operaciones": "16",
          "subtotal": "11441.38 ",
          "descuento": "0.00 ",
          "propina": "1874.00 ",
          "impuestos": "1830.62 ",
          "total": "15146.00 "
      },
      {
          "fecha": "15/01/2021",
          "operaciones": "23",
          "subtotal": "24819.83 ",
          "descuento": "0.00 ",
          "propina": "3724.00 ",
          "impuestos": "3971.17 ",
          "total": "32515.00 "
      },
      {
          "fecha": "15/02/2021",
          "operaciones": "9",
          "subtotal": "6068.10 ",
          "descuento": "288.79 ",
          "propina": "832.00 ",
          "impuestos": "924.69 ",
          "total": "7536.00 "
      },
      {
          "fecha": "15/03/2020",
          "operaciones": "13",
          "subtotal": "13464.66 ",
          "descuento": "839.65 ",
          "propina": "1630.20 ",
          "impuestos": "2020.00 ",
          "total": "16275.21 "
      },
      {
          "fecha": "15/04/2020",
          "operaciones": "4",
          "subtotal": "3850.00 ",
          "descuento": "0.00 ",
          "propina": "622.00 ",
          "impuestos": "616.00 ",
          "total": "5088.00 "
      },
      {
          "fecha": "15/05/2020",
          "operaciones": "10",
          "subtotal": "6602.59 ",
          "descuento": "278.88 ",
          "propina": "462.00 ",
          "impuestos": "1011.79 ",
          "total": "7797.50 "
      },
      {
          "fecha": "15/06/2020",
          "operaciones": "5",
          "subtotal": "4518.97 ",
          "descuento": "0.00 ",
          "propina": "721.00 ",
          "impuestos": "723.03 ",
          "total": "5963.00 "
      },
      {
          "fecha": "15/07/2020",
          "operaciones": "8",
          "subtotal": "11320.69 ",
          "descuento": "0.00 ",
          "propina": "1433.30 ",
          "impuestos": "1811.31 ",
          "total": "14565.30 "
      },
      {
          "fecha": "15/08/2020",
          "operaciones": "24",
          "subtotal": "22064.65 ",
          "descuento": "246.55 ",
          "propina": "2643.00 ",
          "impuestos": "3490.90 ",
          "total": "27952.00 "
      },
      {
          "fecha": "15/09/2020",
          "operaciones": "10",
          "subtotal": "5587.93 ",
          "descuento": "107.76 ",
          "propina": "767.00 ",
          "impuestos": "876.83 ",
          "total": "7124.00 "
      },
      {
          "fecha": "15/10/2020",
          "operaciones": "17",
          "subtotal": "21717.24 ",
          "descuento": "82.76 ",
          "propina": "2513.00 ",
          "impuestos": "3461.52 ",
          "total": "27609.00 "
      },
      {
          "fecha": "15/11/2020",
          "operaciones": "19",
          "subtotal": "18043.96 ",
          "descuento": "729.31 ",
          "propina": "1852.40 ",
          "impuestos": "2770.34 ",
          "total": "21937.40 "
      },
      {
          "fecha": "15/12/2020",
          "operaciones": "24",
          "subtotal": "29516.38 ",
          "descuento": "397.41 ",
          "propina": "3416.70 ",
          "impuestos": "4659.03 ",
          "total": "37194.70 "
      },
      {
          "fecha": "16/01/2021",
          "operaciones": "10",
          "subtotal": "12676.72 ",
          "descuento": "0.00 ",
          "propina": "1832.15 ",
          "impuestos": "2028.28 ",
          "total": "16537.15 "
      },
      {
          "fecha": "16/02/2021",
          "operaciones": "16",
          "subtotal": "16842.24 ",
          "descuento": "137.93 ",
          "propina": "2444.77 ",
          "impuestos": "2672.69 ",
          "total": "21821.77 "
      },
      {
          "fecha": "16/03/2020",
          "operaciones": "15",
          "subtotal": "22154.31 ",
          "descuento": "599.14 ",
          "propina": "2353.00 ",
          "impuestos": "3448.83 ",
          "total": "27357.00 "
      },
      {
          "fecha": "16/04/2020",
          "operaciones": "3",
          "subtotal": "2395.69 ",
          "descuento": "0.00 ",
          "propina": "1339.00 ",
          "impuestos": "383.31 ",
          "total": "4118.00 "
      },
      {
          "fecha": "16/05/2020",
          "operaciones": "7",
          "subtotal": "6673.28 ",
          "descuento": "893.19 ",
          "propina": "145.00 ",
          "impuestos": "924.81 ",
          "total": "6849.90 "
      },
      {
          "fecha": "16/06/2020",
          "operaciones": "1",
          "subtotal": "172.41 ",
          "descuento": "0.00 ",
          "propina": "0.00 ",
          "impuestos": "27.59 ",
          "total": "200.00 "
      },
      {
          "fecha": "16/07/2020",
          "operaciones": "7",
          "subtotal": "8276.72 ",
          "descuento": "265.52 ",
          "propina": "1352.00 ",
          "impuestos": "1281.79 ",
          "total": "10645.00 "
      },
      {
          "fecha": "16/08/2020",
          "operaciones": "20",
          "subtotal": "27328.45 ",
          "descuento": "0.00 ",
          "propina": "2738.40 ",
          "impuestos": "4372.55 ",
          "total": "34439.40 "
      },
      {
          "fecha": "16/09/2020",
          "operaciones": "7",
          "subtotal": "7003.45 ",
          "descuento": "0.00 ",
          "propina": "947.00 ",
          "impuestos": "1120.55 ",
          "total": "9071.00 "
      },
      {
          "fecha": "16/10/2020",
          "operaciones": "19",
          "subtotal": "20041.38 ",
          "descuento": "409.48 ",
          "propina": "2974.50 ",
          "impuestos": "3141.10 ",
          "total": "25747.50 "
      },
      {
          "fecha": "16/11/2020",
          "operaciones": "18",
          "subtotal": "23559.48 ",
          "descuento": "143.10 ",
          "propina": "2787.00 ",
          "impuestos": "3746.62 ",
          "total": "29950.00 "
      },
      {
          "fecha": "16/12/2020",
          "operaciones": "12",
          "subtotal": "17650.00 ",
          "descuento": "0.00 ",
          "propina": "2208.00 ",
          "impuestos": "2824.00 ",
          "total": "22682.00 "
      },
      {
          "fecha": "17/01/2021",
          "operaciones": "12",
          "subtotal": "14665.52 ",
          "descuento": "0.00 ",
          "propina": "1355.00 ",
          "impuestos": "2346.48 ",
          "total": "18367.00 "
      },
      {
          "fecha": "17/02/2021",
          "operaciones": "14",
          "subtotal": "10646.55 ",
          "descuento": "0.00 ",
          "propina": "1539.00 ",
          "impuestos": "1703.45 ",
          "total": "13889.00 "
      },
      {
          "fecha": "17/03/2020",
          "operaciones": "19",
          "subtotal": "14250.86 ",
          "descuento": "825.00 ",
          "propina": "1941.00 ",
          "impuestos": "2148.14 ",
          "total": "17515.00 "
      },
      {
          "fecha": "17/04/2020",
          "operaciones": "8",
          "subtotal": "5733.62 ",
          "descuento": "0.00 ",
          "propina": "897.00 ",
          "impuestos": "917.38 ",
          "total": "7548.00 "
      },
      {
          "fecha": "17/05/2020",
          "operaciones": "8",
          "subtotal": "6908.62 ",
          "descuento": "237.49 ",
          "propina": "944.50 ",
          "impuestos": "1067.38 ",
          "total": "8683.01 "
      },
      {
          "fecha": "17/06/2020",
          "operaciones": "12",
          "subtotal": "15056.03 ",
          "descuento": "106.03 ",
          "propina": "2447.00 ",
          "impuestos": "2392.00 ",
          "total": "19789.00 "
      },
      {
          "fecha": "17/07/2020",
          "operaciones": "18",
          "subtotal": "16045.69 ",
          "descuento": "359.49 ",
          "propina": "2165.00 ",
          "impuestos": "2509.79 ",
          "total": "20360.99 "
      },
      {
          "fecha": "17/08/2020",
          "operaciones": "14",
          "subtotal": "13115.52 ",
          "descuento": "3862.06 ",
          "propina": "1627.00 ",
          "impuestos": "1480.55 ",
          "total": "12361.01 "
      },
      {
          "fecha": "17/09/2020",
          "operaciones": "14",
          "subtotal": "23434.48 ",
          "descuento": "107.76 ",
          "propina": "3378.00 ",
          "impuestos": "3732.28 ",
          "total": "30437.00 "
      },
      {
          "fecha": "17/10/2020",
          "operaciones": "27",
          "subtotal": "28443.10 ",
          "descuento": "2038.80 ",
          "propina": "3087.00 ",
          "impuestos": "4224.69 ",
          "total": "33715.99 "
      },
      {
          "fecha": "17/11/2020",
          "operaciones": "11",
          "subtotal": "12806.90 ",
          "descuento": "304.31 ",
          "propina": "1880.40 ",
          "impuestos": "2000.41 ",
          "total": "16383.40 "
      },
      {
          "fecha": "17/12/2020",
          "operaciones": "16",
          "subtotal": "16265.52 ",
          "descuento": "0.00 ",
          "propina": "1732.45 ",
          "impuestos": "2602.48 ",
          "total": "20600.45 "
      },
      {
          "fecha": "18/01/2021",
          "operaciones": "5",
          "subtotal": "3050.86 ",
          "descuento": "0.00 ",
          "propina": "346.00 ",
          "impuestos": "488.14 ",
          "total": "3885.00 "
      },
      {
          "fecha": "18/02/2021",
          "operaciones": "8",
          "subtotal": "10243.10 ",
          "descuento": "0.00 ",
          "propina": "1451.10 ",
          "impuestos": "1638.90 ",
          "total": "13333.10 "
      },
      {
          "fecha": "18/03/2020",
          "operaciones": "7",
          "subtotal": "3652.59 ",
          "descuento": "549.14 ",
          "propina": "403.00 ",
          "impuestos": "496.55 ",
          "total": "4003.00 "
      },
      {
          "fecha": "18/04/2020",
          "operaciones": "5",
          "subtotal": "2152.59 ",
          "descuento": "0.00 ",
          "propina": "507.00 ",
          "impuestos": "344.41 ",
          "total": "3004.00 "
      },
      {
          "fecha": "18/05/2020",
          "operaciones": "11",
          "subtotal": "9841.38 ",
          "descuento": "141.55 ",
          "propina": "1437.00 ",
          "impuestos": "1551.97 ",
          "total": "12688.80 "
      },
      {
          "fecha": "18/06/2020",
          "operaciones": "6",
          "subtotal": "9696.55 ",
          "descuento": "0.00 ",
          "propina": "2485.00 ",
          "impuestos": "1551.45 ",
          "total": "13733.00 "
      },
      {
          "fecha": "18/07/2020",
          "operaciones": "11",
          "subtotal": "10319.83 ",
          "descuento": "0.00 ",
          "propina": "1450.00 ",
          "impuestos": "1651.17 ",
          "total": "13421.00 "
      },
      {
          "fecha": "18/08/2020",
          "operaciones": "11",
          "subtotal": "12995.69 ",
          "descuento": "0.00 ",
          "propina": "1774.00 ",
          "impuestos": "2079.31 ",
          "total": "16849.00 "
      },
      {
          "fecha": "18/09/2020",
          "operaciones": "10",
          "subtotal": "10333.62 ",
          "descuento": "1005.17 ",
          "propina": "1302.00 ",
          "impuestos": "1492.55 ",
          "total": "12123.00 "
      },
      {
          "fecha": "18/10/2020",
          "operaciones": "18",
          "subtotal": "21676.72 ",
          "descuento": "0.00 ",
          "propina": "2368.70 ",
          "impuestos": "3468.28 ",
          "total": "27513.70 "
      },
      {
          "fecha": "18/11/2020",
          "operaciones": "14",
          "subtotal": "12776.72 ",
          "descuento": "0.00 ",
          "propina": "1322.00 ",
          "impuestos": "2044.28 ",
          "total": "16143.00 "
      },
      {
          "fecha": "18/12/2020",
          "operaciones": "20",
          "subtotal": "23568.10 ",
          "descuento": "0.00 ",
          "propina": "4009.00 ",
          "impuestos": "3770.90 ",
          "total": "31348.00 "
      },
      {
          "fecha": "19/01/2021",
          "operaciones": "15",
          "subtotal": "16123.28 ",
          "descuento": "197.41 ",
          "propina": "2689.14 ",
          "impuestos": "2548.14 ",
          "total": "21163.14 "
      },
      {
          "fecha": "19/02/2021",
          "operaciones": "18",
          "subtotal": "30837.07 ",
          "descuento": "672.42 ",
          "propina": "4274.10 ",
          "impuestos": "4826.34 ",
          "total": "39265.09 "
      },
      {
          "fecha": "19/03/2020",
          "operaciones": "19",
          "subtotal": "22079.31 ",
          "descuento": "811.21 ",
          "propina": "3026.60 ",
          "impuestos": "3402.90 ",
          "total": "27697.60 "
      },
      {
          "fecha": "19/04/2020",
          "operaciones": "9",
          "subtotal": "6449.14 ",
          "descuento": "235.77 ",
          "propina": "513.50 ",
          "impuestos": "994.14 ",
          "total": "7721.01 "
      },
      {
          "fecha": "19/05/2020",
          "operaciones": "1",
          "subtotal": "629.31 ",
          "descuento": "0.00 ",
          "propina": "70.00 ",
          "impuestos": "100.69 ",
          "total": "800.00 "
      },
      {
          "fecha": "19/06/2020",
          "operaciones": "10",
          "subtotal": "6093.10 ",
          "descuento": "0.00 ",
          "propina": "974.00 ",
          "impuestos": "974.90 ",
          "total": "8042.00 "
      },
      {
          "fecha": "19/07/2020",
          "operaciones": "12",
          "subtotal": "15228.45 ",
          "descuento": "0.00 ",
          "propina": "1840.00 ",
          "impuestos": "2436.55 ",
          "total": "19505.00 "
      },
      {
          "fecha": "19/08/2020",
          "operaciones": "19",
          "subtotal": "17571.55 ",
          "descuento": "1164.65 ",
          "propina": "1957.00 ",
          "impuestos": "2625.10 ",
          "total": "20989.00 "
      },
      {
          "fecha": "19/09/2020",
          "operaciones": "27",
          "subtotal": "31416.38 ",
          "descuento": "0.00 ",
          "propina": "3671.00 ",
          "impuestos": "5026.62 ",
          "total": "40114.00 "
      },
      {
          "fecha": "19/10/2020",
          "operaciones": "10",
          "subtotal": "9136.21 ",
          "descuento": "0.00 ",
          "propina": "1218.00 ",
          "impuestos": "1461.79 ",
          "total": "11816.00 "
      },
      {
          "fecha": "19/11/2020",
          "operaciones": "15",
          "subtotal": "21542.24 ",
          "descuento": "525.00 ",
          "propina": "3142.70 ",
          "impuestos": "3362.76 ",
          "total": "27522.70 "
      },
      {
          "fecha": "19/12/2020",
          "operaciones": "11",
          "subtotal": "6797.41 ",
          "descuento": "314.66 ",
          "propina": "636.00 ",
          "impuestos": "1037.24 ",
          "total": "8155.99 "
      },
      {
          "fecha": "20/01/2021",
          "operaciones": "9",
          "subtotal": "15351.72 ",
          "descuento": "0.00 ",
          "propina": "2272.00 ",
          "impuestos": "2456.28 ",
          "total": "20080.00 "
      },
      {
          "fecha": "20/02/2021",
          "operaciones": "28",
          "subtotal": "29775.86 ",
          "descuento": "0.00 ",
          "propina": "3732.05 ",
          "impuestos": "4764.14 ",
          "total": "38272.05 "
      },
      {
          "fecha": "20/03/2020",
          "operaciones": "17",
          "subtotal": "16476.72 ",
          "descuento": "1295.12 ",
          "propina": "1723.60 ",
          "impuestos": "2429.06 ",
          "total": "19334.26 "
      },
      {
          "fecha": "20/04/2020",
          "operaciones": "5",
          "subtotal": "3582.76 ",
          "descuento": "0.00 ",
          "propina": "545.00 ",
          "impuestos": "573.24 ",
          "total": "4701.00 "
      },
      {
          "fecha": "20/05/2020",
          "operaciones": "8",
          "subtotal": "7519.83 ",
          "descuento": "0.00 ",
          "propina": "746.00 ",
          "impuestos": "1203.17 ",
          "total": "9469.00 "
      },
      {
          "fecha": "20/06/2020",
          "operaciones": "8",
          "subtotal": "11212.07 ",
          "descuento": "0.00 ",
          "propina": "1484.00 ",
          "impuestos": "1793.93 ",
          "total": "14490.00 "
      },
      {
          "fecha": "20/07/2020",
          "operaciones": "11",
          "subtotal": "13901.72 ",
          "descuento": "0.00 ",
          "propina": "2059.45 ",
          "impuestos": "2224.28 ",
          "total": "18185.45 "
      },
      {
          "fecha": "20/08/2020",
          "operaciones": "17",
          "subtotal": "16956.90 ",
          "descuento": "1609.48 ",
          "propina": "2148.15 ",
          "impuestos": "2455.59 ",
          "total": "19951.15 "
      },
      {
          "fecha": "20/09/2020",
          "operaciones": "15",
          "subtotal": "13890.52 ",
          "descuento": "1376.72 ",
          "propina": "1316.00 ",
          "impuestos": "2002.21 ",
          "total": "15832.00 "
      },
      {
          "fecha": "20/10/2020",
          "operaciones": "20",
          "subtotal": "19611.21 ",
          "descuento": "431.89 ",
          "propina": "2276.00 ",
          "impuestos": "3068.69 ",
          "total": "24524.01 "
      },
      {
          "fecha": "20/11/2020",
          "operaciones": "18",
          "subtotal": "13708.62 ",
          "descuento": "0.00 ",
          "propina": "1929.80 ",
          "impuestos": "2193.38 ",
          "total": "17831.80 "
      },
      {
          "fecha": "20/12/2020",
          "operaciones": "10",
          "subtotal": "8006.03 ",
          "descuento": "0.00 ",
          "propina": "1112.00 ",
          "impuestos": "1280.97 ",
          "total": "10399.00 "
      },
      {
          "fecha": "21/01/2021",
          "operaciones": "11",
          "subtotal": "7768.10 ",
          "descuento": "170.69 ",
          "propina": "844.00 ",
          "impuestos": "1215.59 ",
          "total": "9657.00 "
      },
      {
          "fecha": "21/02/2021",
          "operaciones": "12",
          "subtotal": "15225.00 ",
          "descuento": "0.00 ",
          "propina": "2065.00 ",
          "impuestos": "2436.00 ",
          "total": "19726.00 "
      },
      {
          "fecha": "21/03/2020",
          "operaciones": "16",
          "subtotal": "14060.34 ",
          "descuento": "0.00 ",
          "propina": "1563.00 ",
          "impuestos": "2249.66 ",
          "total": "17873.00 "
      },
      {
          "fecha": "21/04/2020",
          "operaciones": "7",
          "subtotal": "6071.55 ",
          "descuento": "0.00 ",
          "propina": "778.00 ",
          "impuestos": "971.45 ",
          "total": "7821.00 "
      },
      {
          "fecha": "21/05/2020",
          "operaciones": "7",
          "subtotal": "5130.17 ",
          "descuento": "5.60 ",
          "propina": "711.50 ",
          "impuestos": "819.93 ",
          "total": "6656.00 "
      },
      {
          "fecha": "21/06/2020",
          "operaciones": "24",
          "subtotal": "24606.03 ",
          "descuento": "0.00 ",
          "propina": "2005.00 ",
          "impuestos": "3936.97 ",
          "total": "30548.00 "
      },
      {
          "fecha": "21/07/2020",
          "operaciones": "10",
          "subtotal": "7291.38 ",
          "descuento": "0.00 ",
          "propina": "1321.00 ",
          "impuestos": "1166.62 ",
          "total": "9779.00 "
      },
      {
          "fecha": "21/08/2020",
          "operaciones": "19",
          "subtotal": "21473.27 ",
          "descuento": "0.00 ",
          "propina": "2731.70 ",
          "impuestos": "3435.72 ",
          "total": "27640.70 "
      },
      {
          "fecha": "21/09/2020",
          "operaciones": "4",
          "subtotal": "3498.28 ",
          "descuento": "0.00 ",
          "propina": "410.00 ",
          "impuestos": "559.72 ",
          "total": "4468.00 "
      },
      {
          "fecha": "21/10/2020",
          "operaciones": "12",
          "subtotal": "6054.31 ",
          "descuento": "0.00 ",
          "propina": "682.00 ",
          "impuestos": "968.69 ",
          "total": "7705.00 "
      },
      {
          "fecha": "21/11/2020",
          "operaciones": "23",
          "subtotal": "31291.38 ",
          "descuento": "2159.48 ",
          "propina": "3701.00 ",
          "impuestos": "4661.10 ",
          "total": "37494.00 "
      },
      {
          "fecha": "21/12/2020",
          "operaciones": "12",
          "subtotal": "11151.72 ",
          "descuento": "0.00 ",
          "propina": "1803.00 ",
          "impuestos": "1784.28 ",
          "total": "14739.00 "
      },
      {
          "fecha": "22/01/2021",
          "operaciones": "18",
          "subtotal": "18120.69 ",
          "descuento": "0.00 ",
          "propina": "2698.00 ",
          "impuestos": "2899.31 ",
          "total": "23718.00 "
      },
      {
          "fecha": "22/02/2021",
          "operaciones": "11",
          "subtotal": "5842.24 ",
          "descuento": "0.00 ",
          "propina": "881.00 ",
          "impuestos": "934.76 ",
          "total": "7658.00 "
      },
      {
          "fecha": "22/03/2020",
          "operaciones": "18",
          "subtotal": "24007.76 ",
          "descuento": "621.55 ",
          "propina": "3057.05 ",
          "impuestos": "3741.79 ",
          "total": "30185.05 "
      },
      {
          "fecha": "22/04/2020",
          "operaciones": "3",
          "subtotal": "2616.38 ",
          "descuento": "31.29 ",
          "propina": "500.00 ",
          "impuestos": "413.61 ",
          "total": "3498.70 "
      },
      {
          "fecha": "22/05/2020",
          "operaciones": "8",
          "subtotal": "9762.93 ",
          "descuento": "2824.14 ",
          "propina": "2250.00 ",
          "impuestos": "1110.21 ",
          "total": "10299.00 "
      },
      {
          "fecha": "22/06/2020",
          "operaciones": "3",
          "subtotal": "4141.38 ",
          "descuento": "0.00 ",
          "propina": "700.00 ",
          "impuestos": "662.62 ",
          "total": "5504.00 "
      },
      {
          "fecha": "22/07/2020",
          "operaciones": "11",
          "subtotal": "8255.17 ",
          "descuento": "0.00 ",
          "propina": "1039.00 ",
          "impuestos": "1320.83 ",
          "total": "10615.00 "
      },
      {
          "fecha": "22/08/2020",
          "operaciones": "18",
          "subtotal": "15955.17 ",
          "descuento": "152.59 ",
          "propina": "1883.00 ",
          "impuestos": "2528.41 ",
          "total": "20214.00 "
      },
      {
          "fecha": "22/09/2020",
          "operaciones": "20",
          "subtotal": "12340.52 ",
          "descuento": "444.83 ",
          "propina": "1528.00 ",
          "impuestos": "1903.31 ",
          "total": "15327.00 "
      },
      {
          "fecha": "22/10/2020",
          "operaciones": "18",
          "subtotal": "13723.28 ",
          "descuento": "408.62 ",
          "propina": "1542.00 ",
          "impuestos": "2130.34 ",
          "total": "16987.00 "
      },
      {
          "fecha": "22/11/2020",
          "operaciones": "11",
          "subtotal": "11762.93 ",
          "descuento": "0.00 ",
          "propina": "1530.00 ",
          "impuestos": "1882.07 ",
          "total": "15175.00 "
      },
      {
          "fecha": "22/12/2020",
          "operaciones": "9",
          "subtotal": "10181.90 ",
          "descuento": "0.00 ",
          "propina": "1146.00 ",
          "impuestos": "1629.10 ",
          "total": "12957.00 "
      },
      {
          "fecha": "23/01/2021",
          "operaciones": "13",
          "subtotal": "9537.07 ",
          "descuento": "0.00 ",
          "propina": "1307.10 ",
          "impuestos": "1525.93 ",
          "total": "12370.10 "
      },
      {
          "fecha": "23/02/2021",
          "operaciones": "14",
          "subtotal": "11901.72 ",
          "descuento": "689.66 ",
          "propina": "1380.00 ",
          "impuestos": "1793.93 ",
          "total": "14385.99 "
      },
      {
          "fecha": "23/03/2020",
          "operaciones": "13",
          "subtotal": "6210.34 ",
          "descuento": "553.45 ",
          "propina": "788.00 ",
          "impuestos": "905.10 ",
          "total": "7350.00 "
      },
      {
          "fecha": "23/04/2020",
          "operaciones": "4",
          "subtotal": "4875.00 ",
          "descuento": "0.00 ",
          "propina": "944.00 ",
          "impuestos": "780.00 ",
          "total": "6599.00 "
      },
      {
          "fecha": "23/05/2020",
          "operaciones": "7",
          "subtotal": "5300.86 ",
          "descuento": "116.46 ",
          "propina": "958.00 ",
          "impuestos": "829.50 ",
          "total": "6971.91 "
      },
      {
          "fecha": "23/06/2020",
          "operaciones": "10",
          "subtotal": "12090.52 ",
          "descuento": "0.00 ",
          "propina": "2776.00 ",
          "impuestos": "1934.48 ",
          "total": "16801.00 "
      },
      {
          "fecha": "23/07/2020",
          "operaciones": "14",
          "subtotal": "13245.69 ",
          "descuento": "0.00 ",
          "propina": "2031.80 ",
          "impuestos": "2119.31 ",
          "total": "17396.80 "
      },
      {
          "fecha": "23/08/2020",
          "operaciones": "20",
          "subtotal": "21635.34 ",
          "descuento": "0.00 ",
          "propina": "2742.00 ",
          "impuestos": "3461.66 ",
          "total": "27839.00 "
      },
      {
          "fecha": "23/09/2020",
          "operaciones": "19",
          "subtotal": "14135.34 ",
          "descuento": "669.83 ",
          "propina": "1660.00 ",
          "impuestos": "2154.48 ",
          "total": "17280.00 "
      },
      {
          "fecha": "23/10/2020",
          "operaciones": "26",
          "subtotal": "33718.10 ",
          "descuento": "0.00 ",
          "propina": "4802.40 ",
          "impuestos": "5394.90 ",
          "total": "43915.40 "
      },
      {
          "fecha": "23/11/2020",
          "operaciones": "16",
          "subtotal": "12615.52 ",
          "descuento": "525.87 ",
          "propina": "1448.00 ",
          "impuestos": "1934.34 ",
          "total": "15471.99 "
      },
      {
          "fecha": "23/12/2020",
          "operaciones": "11",
          "subtotal": "17794.83 ",
          "descuento": "0.00 ",
          "propina": "2273.00 ",
          "impuestos": "2847.17 ",
          "total": "22915.00 "
      },
      {
          "fecha": "24/01/2021",
          "operaciones": "25",
          "subtotal": "22894.83 ",
          "descuento": "671.55 ",
          "propina": "2798.00 ",
          "impuestos": "3555.72 ",
          "total": "28577.00 "
      },
      {
          "fecha": "24/02/2021",
          "operaciones": "10",
          "subtotal": "9738.79 ",
          "descuento": "453.45 ",
          "propina": "1119.00 ",
          "impuestos": "1485.66 ",
          "total": "11890.00 "
      },
      {
          "fecha": "24/03/2020",
          "operaciones": "8",
          "subtotal": "6643.10 ",
          "descuento": "0.00 ",
          "propina": "1204.00 ",
          "impuestos": "1062.90 ",
          "total": "8910.00 "
      },
      {
          "fecha": "24/04/2020",
          "operaciones": "4",
          "subtotal": "1530.17 ",
          "descuento": "0.00 ",
          "propina": "30.00 ",
          "impuestos": "244.83 ",
          "total": "1805.00 "
      },
      {
          "fecha": "24/05/2020",
          "operaciones": "10",
          "subtotal": "9627.59 ",
          "descuento": "112.93 ",
          "propina": "1239.00 ",
          "impuestos": "1522.34 ",
          "total": "12276.00 "
      },
      {
          "fecha": "24/06/2020",
          "operaciones": "8",
          "subtotal": "10483.62 ",
          "descuento": "0.00 ",
          "propina": "1167.00 ",
          "impuestos": "1677.38 ",
          "total": "13328.00 "
      },
      {
          "fecha": "24/07/2020",
          "operaciones": "19",
          "subtotal": "16106.03 ",
          "descuento": "485.34 ",
          "propina": "2266.00 ",
          "impuestos": "2499.31 ",
          "total": "20386.00 "
      },
      {
          "fecha": "24/08/2020",
          "operaciones": "5",
          "subtotal": "3915.52 ",
          "descuento": "0.00 ",
          "propina": "468.00 ",
          "impuestos": "626.48 ",
          "total": "5010.00 "
      },
      {
          "fecha": "24/09/2020",
          "operaciones": "16",
          "subtotal": "13050.00 ",
          "descuento": "0.00 ",
          "propina": "1596.00 ",
          "impuestos": "2088.00 ",
          "total": "16734.00 "
      },
      {
          "fecha": "24/10/2020",
          "operaciones": "32",
          "subtotal": "46193.10 ",
          "descuento": "2646.55 ",
          "propina": "6268.00 ",
          "impuestos": "6967.45 ",
          "total": "56782.00 "
      },
      {
          "fecha": "24/11/2020",
          "operaciones": "14",
          "subtotal": "13916.38 ",
          "descuento": "219.83 ",
          "propina": "1894.20 ",
          "impuestos": "2191.45 ",
          "total": "17782.20 "
      },
      {
          "fecha": "24/12/2020",
          "operaciones": "30",
          "subtotal": "30608.62 ",
          "descuento": "0.00 ",
          "propina": "1455.00 ",
          "impuestos": "4897.38 ",
          "total": "36961.00 "
      },
      {
          "fecha": "25/01/2021",
          "operaciones": "9",
          "subtotal": "7041.38 ",
          "descuento": "590.52 ",
          "propina": "918.00 ",
          "impuestos": "1032.14 ",
          "total": "8401.00 "
      },
      {
          "fecha": "25/02/2021",
          "operaciones": "17",
          "subtotal": "13574.14 ",
          "descuento": "481.90 ",
          "propina": "1798.25 ",
          "impuestos": "2094.76 ",
          "total": "16985.25 "
      },
      {
          "fecha": "25/03/2020",
          "operaciones": "13",
          "subtotal": "8828.45 ",
          "descuento": "446.55 ",
          "propina": "1006.00 ",
          "impuestos": "1341.10 ",
          "total": "10729.00 "
      },
      {
          "fecha": "25/04/2020",
          "operaciones": "9",
          "subtotal": "7850.00 ",
          "descuento": "662.50 ",
          "propina": "645.50 ",
          "impuestos": "1150.00 ",
          "total": "8983.00 "
      },
      {
          "fecha": "25/05/2020",
          "operaciones": "4",
          "subtotal": "3775.00 ",
          "descuento": "0.00 ",
          "propina": "768.00 ",
          "impuestos": "604.00 ",
          "total": "5147.00 "
      },
      {
          "fecha": "25/06/2020",
          "operaciones": "7",
          "subtotal": "8720.69 ",
          "descuento": "0.00 ",
          "propina": "1446.00 ",
          "impuestos": "1395.31 ",
          "total": "11562.00 "
      },
      {
          "fecha": "25/07/2020",
          "operaciones": "15",
          "subtotal": "13278.45 ",
          "descuento": "968.97 ",
          "propina": "1749.75 ",
          "impuestos": "1969.52 ",
          "total": "16028.74 "
      },
      {
          "fecha": "25/08/2020",
          "operaciones": "7",
          "subtotal": "5056.03 ",
          "descuento": "189.66 ",
          "propina": "589.00 ",
          "impuestos": "778.62 ",
          "total": "6234.00 "
      },
      {
          "fecha": "25/09/2020",
          "operaciones": "35",
          "subtotal": "46690.52 ",
          "descuento": "893.11 ",
          "propina": "5244.00 ",
          "impuestos": "7327.59 ",
          "total": "58368.99 "
      },
      {
          "fecha": "25/10/2020",
          "operaciones": "22",
          "subtotal": "19845.69 ",
          "descuento": "0.00 ",
          "propina": "2054.00 ",
          "impuestos": "3175.31 ",
          "total": "25075.00 "
      },
      {
          "fecha": "25/11/2020",
          "operaciones": "16",
          "subtotal": "16200.00 ",
          "descuento": "0.00 ",
          "propina": "1636.50 ",
          "impuestos": "2592.00 ",
          "total": "20428.50 "
      },
      {
          "fecha": "26/01/2021",
          "operaciones": "12",
          "subtotal": "10050.00 ",
          "descuento": "742.24 ",
          "propina": "1483.00 ",
          "impuestos": "1489.24 ",
          "total": "12280.00 "
      },
      {
          "fecha": "26/02/2021",
          "operaciones": "23",
          "subtotal": "28545.69 ",
          "descuento": "0.00 ",
          "propina": "4022.05 ",
          "impuestos": "4567.31 ",
          "total": "37135.05 "
      },
      {
          "fecha": "26/03/2020",
          "operaciones": "9",
          "subtotal": "9284.48 ",
          "descuento": "64.66 ",
          "propina": "1313.00 ",
          "impuestos": "1475.17 ",
          "total": "12008.00 "
      },
      {
          "fecha": "26/04/2020",
          "operaciones": "13",
          "subtotal": "7195.69 ",
          "descuento": "88.01 ",
          "propina": "1015.00 ",
          "impuestos": "1137.23 ",
          "total": "9259.91 "
      },
      {
          "fecha": "26/05/2020",
          "operaciones": "6",
          "subtotal": "7106.03 ",
          "descuento": "0.00 ",
          "propina": "1005.00 ",
          "impuestos": "1136.97 ",
          "total": "9248.00 "
      },
      {
          "fecha": "26/06/2020",
          "operaciones": "10",
          "subtotal": "13250.00 ",
          "descuento": "0.00 ",
          "propina": "2071.00 ",
          "impuestos": "2120.00 ",
          "total": "17441.00 "
      },
      {
          "fecha": "26/07/2020",
          "operaciones": "16",
          "subtotal": "20546.55 ",
          "descuento": "0.00 ",
          "propina": "2582.00 ",
          "impuestos": "3287.45 ",
          "total": "26416.00 "
      },
      {
          "fecha": "26/08/2020",
          "operaciones": "17",
          "subtotal": "12456.03 ",
          "descuento": "0.00 ",
          "propina": "1569.00 ",
          "impuestos": "1992.97 ",
          "total": "16018.00 "
      },
      {
          "fecha": "26/09/2020",
          "operaciones": "28",
          "subtotal": "27094.83 ",
          "descuento": "612.93 ",
          "propina": "3226.00 ",
          "impuestos": "4237.10 ",
          "total": "33945.00 "
      },
      {
          "fecha": "26/10/2020",
          "operaciones": "12",
          "subtotal": "11119.83 ",
          "descuento": "278.45 ",
          "propina": "1723.00 ",
          "impuestos": "1734.62 ",
          "total": "14299.00 "
      },
      {
          "fecha": "26/11/2020",
          "operaciones": "11",
          "subtotal": "13667.24 ",
          "descuento": "0.00 ",
          "propina": "1733.00 ",
          "impuestos": "2186.76 ",
          "total": "17587.00 "
      },
      {
          "fecha": "26/12/2020",
          "operaciones": "5",
          "subtotal": "9029.31 ",
          "descuento": "0.00 ",
          "propina": "1437.70 ",
          "impuestos": "1444.69 ",
          "total": "11911.70 "
      },
      {
          "fecha": "27/01/2021",
          "operaciones": "9",
          "subtotal": "12123.28 ",
          "descuento": "0.00 ",
          "propina": "1929.00 ",
          "impuestos": "1939.72 ",
          "total": "15992.00 "
      },
      {
          "fecha": "27/02/2021",
          "operaciones": "29",
          "subtotal": "39696.55 ",
          "descuento": "0.00 ",
          "propina": "4936.20 ",
          "impuestos": "6351.45 ",
          "total": "50984.20 "
      },
      {
          "fecha": "27/03/2020",
          "operaciones": "3",
          "subtotal": "2434.48 ",
          "descuento": "0.00 ",
          "propina": "200.00 ",
          "impuestos": "389.52 ",
          "total": "3024.00 "
      },
      {
          "fecha": "27/04/2020",
          "operaciones": "4",
          "subtotal": "4568.97 ",
          "descuento": "0.00 ",
          "propina": "637.00 ",
          "impuestos": "731.03 ",
          "total": "5937.00 "
      },
      {
          "fecha": "27/05/2020",
          "operaciones": "3",
          "subtotal": "2644.83 ",
          "descuento": "0.00 ",
          "propina": "434.00 ",
          "impuestos": "423.17 ",
          "total": "3502.00 "
      },
      {
          "fecha": "27/06/2020",
          "operaciones": "8",
          "subtotal": "6242.24 ",
          "descuento": "69.40 ",
          "propina": "752.00 ",
          "impuestos": "987.66 ",
          "total": "7912.50 "
      },
      {
          "fecha": "27/07/2020",
          "operaciones": "9",
          "subtotal": "7098.28 ",
          "descuento": "0.00 ",
          "propina": "1369.00 ",
          "impuestos": "1135.72 ",
          "total": "9603.00 "
      },
      {
          "fecha": "27/08/2020",
          "operaciones": "11",
          "subtotal": "13554.31 ",
          "descuento": "151.72 ",
          "propina": "1481.80 ",
          "impuestos": "2144.41 ",
          "total": "17028.80 "
      },
      {
          "fecha": "27/09/2020",
          "operaciones": "13",
          "subtotal": "10729.31 ",
          "descuento": "0.00 ",
          "propina": "1237.00 ",
          "impuestos": "1716.69 ",
          "total": "13683.00 "
      },
      {
          "fecha": "27/10/2020",
          "operaciones": "20",
          "subtotal": "29623.27 ",
          "descuento": "0.00 ",
          "propina": "4209.50 ",
          "impuestos": "4739.72 ",
          "total": "38572.50 "
      },
      {
          "fecha": "27/11/2020",
          "operaciones": "32",
          "subtotal": "39462.93 ",
          "descuento": "670.69 ",
          "propina": "5613.45 ",
          "impuestos": "6206.76 ",
          "total": "50612.45 "
      },
      {
          "fecha": "27/12/2020",
          "operaciones": "5",
          "subtotal": "3746.55 ",
          "descuento": "0.00 ",
          "propina": "510.00 ",
          "impuestos": "599.45 ",
          "total": "4856.00 "
      },
      {
          "fecha": "28/01/2021",
          "operaciones": "8",
          "subtotal": "8857.76 ",
          "descuento": "0.00 ",
          "propina": "1025.00 ",
          "impuestos": "1417.24 ",
          "total": "11300.00 "
      },
      {
          "fecha": "28/02/2021",
          "operaciones": "12",
          "subtotal": "13896.55 ",
          "descuento": "856.03 ",
          "propina": "1580.00 ",
          "impuestos": "2086.48 ",
          "total": "16707.00 "
      },
      {
          "fecha": "28/03/2020",
          "operaciones": "7",
          "subtotal": "4600.00 ",
          "descuento": "90.52 ",
          "propina": "577.00 ",
          "impuestos": "721.52 ",
          "total": "5808.00 "
      },
      {
          "fecha": "28/04/2020",
          "operaciones": "1",
          "subtotal": "981.03 ",
          "descuento": "98.10 ",
          "propina": "175.80 ",
          "impuestos": "141.27 ",
          "total": "1200.00 "
      },
      {
          "fecha": "28/05/2020",
          "operaciones": "7",
          "subtotal": "3309.48 ",
          "descuento": "0.00 ",
          "propina": "877.95 ",
          "impuestos": "529.52 ",
          "total": "4716.95 "
      },
      {
          "fecha": "28/06/2020",
          "operaciones": "9",
          "subtotal": "9748.28 ",
          "descuento": "595.26 ",
          "propina": "992.50 ",
          "impuestos": "1464.48 ",
          "total": "11610.00 "
      },
      {
          "fecha": "28/07/2020",
          "operaciones": "13",
          "subtotal": "11698.28 ",
          "descuento": "0.00 ",
          "propina": "1587.00 ",
          "impuestos": "1871.72 ",
          "total": "15157.00 "
      },
      {
          "fecha": "28/08/2020",
          "operaciones": "20",
          "subtotal": "24796.55 ",
          "descuento": "0.00 ",
          "propina": "4005.00 ",
          "impuestos": "3967.45 ",
          "total": "32769.00 "
      },
      {
          "fecha": "28/09/2020",
          "operaciones": "15",
          "subtotal": "13767.24 ",
          "descuento": "0.00 ",
          "propina": "1731.00 ",
          "impuestos": "2202.76 ",
          "total": "17701.00 "
      },
      {
          "fecha": "28/10/2020",
          "operaciones": "19",
          "subtotal": "23414.66 ",
          "descuento": "83.62 ",
          "propina": "3264.20 ",
          "impuestos": "3732.97 ",
          "total": "30328.20 "
      },
      {
          "fecha": "28/11/2020",
          "operaciones": "21",
          "subtotal": "18614.65 ",
          "descuento": "347.41 ",
          "propina": "2320.00 ",
          "impuestos": "2922.76 ",
          "total": "23510.00 "
      },
      {
          "fecha": "28/12/2020",
          "operaciones": "7",
          "subtotal": "5054.31 ",
          "descuento": "698.28 ",
          "propina": "594.00 ",
          "impuestos": "696.97 ",
          "total": "5647.00 "
      },
      {
          "fecha": "29/01/2021",
          "operaciones": "15",
          "subtotal": "13982.41 ",
          "descuento": "0.00 ",
          "propina": "1860.00 ",
          "impuestos": "2237.19 ",
          "total": "18079.60 "
      },
      {
          "fecha": "29/03/2020",
          "operaciones": "2",
          "subtotal": "448.28 ",
          "descuento": "0.00 ",
          "propina": "40.00 ",
          "impuestos": "71.72 ",
          "total": "560.00 "
      },
      {
          "fecha": "29/04/2020",
          "operaciones": "7",
          "subtotal": "4231.03 ",
          "descuento": "0.00 ",
          "propina": "670.00 ",
          "impuestos": "676.97 ",
          "total": "5578.00 "
      },
      {
          "fecha": "29/05/2020",
          "operaciones": "5",
          "subtotal": "4853.45 ",
          "descuento": "30.00 ",
          "propina": "713.00 ",
          "impuestos": "771.75 ",
          "total": "6308.20 "
      },
      {
          "fecha": "29/06/2020",
          "operaciones": "6",
          "subtotal": "10234.48 ",
          "descuento": "0.00 ",
          "propina": "1402.00 ",
          "impuestos": "1637.52 ",
          "total": "13274.00 "
      },
      {
          "fecha": "29/07/2020",
          "operaciones": "13",
          "subtotal": "13450.86 ",
          "descuento": "0.00 ",
          "propina": "1949.55 ",
          "impuestos": "2152.14 ",
          "total": "17552.55 "
      },
      {
          "fecha": "29/08/2020",
          "operaciones": "31",
          "subtotal": "42825.86 ",
          "descuento": "2674.14 ",
          "propina": "4225.00 ",
          "impuestos": "6424.28 ",
          "total": "50801.00 "
      },
      {
          "fecha": "29/09/2020",
          "operaciones": "18",
          "subtotal": "20943.10 ",
          "descuento": "0.00 ",
          "propina": "2292.00 ",
          "impuestos": "3350.90 ",
          "total": "26586.00 "
      },
      {
          "fecha": "29/10/2020",
          "operaciones": "25",
          "subtotal": "24118.10 ",
          "descuento": "152.59 ",
          "propina": "2965.00 ",
          "impuestos": "3834.48 ",
          "total": "30765.00 "
      },
      {
          "fecha": "29/11/2020",
          "operaciones": "22",
          "subtotal": "23184.48 ",
          "descuento": "0.00 ",
          "propina": "2404.00 ",
          "impuestos": "3709.52 ",
          "total": "29298.00 "
      },
      {
          "fecha": "29/12/2020",
          "operaciones": "4",
          "subtotal": "5353.45 ",
          "descuento": "0.00 ",
          "propina": "900.00 ",
          "impuestos": "856.55 ",
          "total": "7110.00 "
      },
      {
          "fecha": "30/01/2021",
          "operaciones": "12",
          "subtotal": "10989.66 ",
          "descuento": "0.00 ",
          "propina": "1581.00 ",
          "impuestos": "1758.34 ",
          "total": "14329.00 "
      },
      {
          "fecha": "30/04/2020",
          "operaciones": "10",
          "subtotal": "9856.03 ",
          "descuento": "0.00 ",
          "propina": "1478.00 ",
          "impuestos": "1576.97 ",
          "total": "12911.00 "
      },
      {
          "fecha": "30/05/2020",
          "operaciones": "8",
          "subtotal": "7653.45 ",
          "descuento": "0.00 ",
          "propina": "926.00 ",
          "impuestos": "1224.55 ",
          "total": "9804.00 "
      },
      {
          "fecha": "30/06/2020",
          "operaciones": "9",
          "subtotal": "11383.62 ",
          "descuento": "1210.34 ",
          "propina": "1788.00 ",
          "impuestos": "1627.72 ",
          "total": "13589.00 "
      },
      {
          "fecha": "30/07/2020",
          "operaciones": "11",
          "subtotal": "13686.21 ",
          "descuento": "0.00 ",
          "propina": "1953.90 ",
          "impuestos": "2189.79 ",
          "total": "17829.90 "
      },
      {
          "fecha": "30/08/2020",
          "operaciones": "20",
          "subtotal": "18546.55 ",
          "descuento": "351.72 ",
          "propina": "2296.00 ",
          "impuestos": "2911.17 ",
          "total": "23402.00 "
      },
      {
          "fecha": "30/09/2020",
          "operaciones": "14",
          "subtotal": "15087.07 ",
          "descuento": "128.45 ",
          "propina": "1737.00 ",
          "impuestos": "2393.38 ",
          "total": "19089.00 "
      },
      {
          "fecha": "30/10/2020",
          "operaciones": "18",
          "subtotal": "16896.55 ",
          "descuento": "0.00 ",
          "propina": "2204.75 ",
          "impuestos": "2703.45 ",
          "total": "21804.75 "
      },
      {
          "fecha": "30/11/2020",
          "operaciones": "20",
          "subtotal": "15108.62 ",
          "descuento": "151.72 ",
          "propina": "1823.00 ",
          "impuestos": "2393.10 ",
          "total": "19173.00 "
      },
      {
          "fecha": "30/12/2020",
          "operaciones": "13",
          "subtotal": "11262.93 ",
          "descuento": "165.52 ",
          "propina": "913.00 ",
          "impuestos": "1775.59 ",
          "total": "13786.00 "
      },
      {
          "fecha": "31/01/2021",
          "operaciones": "23",
          "subtotal": "16664.65 ",
          "descuento": "81.90 ",
          "propina": "1470.00 ",
          "impuestos": "2653.24 ",
          "total": "20706.00 "
      },
      {
          "fecha": "31/03/2020",
          "operaciones": "5",
          "subtotal": "8211.21 ",
          "descuento": "0.00 ",
          "propina": "1427.00 ",
          "impuestos": "1313.79 ",
          "total": "10952.00 "
      },
      {
          "fecha": "31/05/2020",
          "operaciones": "12",
          "subtotal": "15907.76 ",
          "descuento": "2966.38 ",
          "propina": "1674.00 ",
          "impuestos": "2070.62 ",
          "total": "16686.00 "
      },
      {
          "fecha": "31/07/2020",
          "operaciones": "20",
          "subtotal": "21078.45 ",
          "descuento": "355.17 ",
          "propina": "3211.00 ",
          "impuestos": "3315.72 ",
          "total": "27250.00 "
      },
      {
          "fecha": "31/08/2020",
          "operaciones": "8",
          "subtotal": "6892.24 ",
          "descuento": "0.00 ",
          "propina": "772.00 ",
          "impuestos": "1102.76 ",
          "total": "8767.00 "
      },
      {
          "fecha": "31/10/2020",
          "operaciones": "24",
          "subtotal": "25802.58 ",
          "descuento": "165.52 ",
          "propina": "3064.00 ",
          "impuestos": "4101.93 ",
          "total": "32803.00 "
      },
      {
          "fecha": "31/12/2020",
          "operaciones": "35",
          "subtotal": "34853.45 ",
          "descuento": "107.93 ",
          "propina": "2064.00 ",
          "impuestos": "5559.28 ",
          "total": "42368.80 "
      }
  ]
