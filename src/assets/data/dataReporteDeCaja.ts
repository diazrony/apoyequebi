export const dataReporteDeCaja = [
      {
          "fecha": "01/03/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-7,773",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7773.00)"
      },
      {
          "fecha": "01/03/2021",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "7,773",
          "tarjetaCredito": "6567.00 ",
          "tarjetaDebito": "9,642",
          "total": "23982.00 "
      },
      {
          "fecha": "28/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,266",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5266.00)"
      },
      {
          "fecha": "28/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "5,266",
          "tarjetaCredito": "1854.00 ",
          "tarjetaDebito": "9,587",
          "total": "16707.00 "
      },
      {
          "fecha": "27/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "29.00 ",
          "efectivo": "13,130",
          "tarjetaCredito": "11415.20 ",
          "tarjetaDebito": "26,439",
          "total": "50984.20 "
      },
      {
          "fecha": "27/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-13,130",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13130.00)"
      },
      {
          "fecha": "26/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-11,581",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11581.00)"
      },
      {
          "fecha": "26/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "23.00 ",
          "efectivo": "7,441",
          "tarjetaCredito": "15995.05 ",
          "tarjetaDebito": "13,699",
          "total": "37135.05 "
      },
      {
          "fecha": "25/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,765",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1765.00)"
      },
      {
          "fecha": "25/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "5,905",
          "tarjetaCredito": "5957.25 ",
          "tarjetaDebito": "5,123",
          "total": "16985.25 "
      },
      {
          "fecha": "24/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "4,570",
          "tarjetaCredito": "6235.00 ",
          "tarjetaDebito": "1,085",
          "total": "11890.00 "
      },
      {
          "fecha": "24/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,570",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4570.00)"
      },
      {
          "fecha": "23/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "3,617",
          "tarjetaCredito": "6764.00 ",
          "tarjetaDebito": "4,005",
          "total": "14386.00 "
      },
      {
          "fecha": "23/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-3,617",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3617.00)"
      },
      {
          "fecha": "22/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,256",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3256.00)"
      },
      {
          "fecha": "22/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "3,256",
          "tarjetaCredito": "1382.00 ",
          "tarjetaDebito": "3,020",
          "total": "7658.00 "
      },
      {
          "fecha": "21/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "8,042",
          "tarjetaCredito": "7318.00 ",
          "tarjetaDebito": "4,366",
          "total": "19726.00 "
      },
      {
          "fecha": "21/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-8,042",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8042.00)"
      },
      {
          "fecha": "20/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "8.00 ",
          "efectivo": "-11,934",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11934.00)"
      },
      {
          "fecha": "20/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "28.00 ",
          "efectivo": "11,934",
          "tarjetaCredito": "6794.05 ",
          "tarjetaDebito": "19,544",
          "total": "38272.05 "
      },
      {
          "fecha": "19/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "2,500",
          "tarjetaCredito": "4833.10 ",
          "tarjetaDebito": "31,932",
          "total": "39265.10 "
      },
      {
          "fecha": "19/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-2,500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2500.00)"
      },
      {
          "fecha": "18/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,229",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4229.00)"
      },
      {
          "fecha": "18/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "4,229",
          "tarjetaCredito": "3417.10 ",
          "tarjetaDebito": "5,687",
          "total": "13333.10 "
      },
      {
          "fecha": "17/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,585",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4585.00)"
      },
      {
          "fecha": "17/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "4,585",
          "tarjetaCredito": "5584.00 ",
          "tarjetaDebito": "3,720",
          "total": "13889.00 "
      },
      {
          "fecha": "16/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,214",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4214.00)"
      },
      {
          "fecha": "16/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "4,214",
          "tarjetaCredito": "15535.77 ",
          "tarjetaDebito": "2,072",
          "total": "21821.77 "
      },
      {
          "fecha": "15/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-2,616",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2616.00)"
      },
      {
          "fecha": "15/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "2,616",
          "tarjetaCredito": "2142.00 ",
          "tarjetaDebito": "2,778",
          "total": "7536.00 "
      },
      {
          "fecha": "14/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "56.00 ",
          "efectivo": "28,197",
          "tarjetaCredito": "13432.00 ",
          "tarjetaDebito": "34,760",
          "total": "76389.00 "
      },
      {
          "fecha": "14/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "8.00 ",
          "efectivo": "-28,197",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(28197.00)"
      },
      {
          "fecha": "13/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-8,809",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8809.00)"
      },
      {
          "fecha": "13/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "46.00 ",
          "efectivo": "8,809",
          "tarjetaCredito": "37616.05 ",
          "tarjetaDebito": "21,805",
          "total": "68230.05 "
      },
      {
          "fecha": "12/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "3,631",
          "tarjetaCredito": "8281.20 ",
          "tarjetaDebito": "12,473",
          "total": "24385.20 "
      },
      {
          "fecha": "12/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,631",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3631.00)"
      },
      {
          "fecha": "11/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "1,610",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,872",
          "total": "5482.00 "
      },
      {
          "fecha": "11/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,610",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1610.00)"
      },
      {
          "fecha": "10/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-7,790",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7790.00)"
      },
      {
          "fecha": "10/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "7,790",
          "tarjetaCredito": "10461.00 ",
          "tarjetaDebito": "565",
          "total": "18816.00 "
      },
      {
          "fecha": "09/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "5,125",
          "tarjetaCredito": "1335.00 ",
          "tarjetaDebito": "3,548",
          "total": "10008.00 "
      },
      {
          "fecha": "09/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,125",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5125.00)"
      },
      {
          "fecha": "08/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-5,445",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5445.00)"
      },
      {
          "fecha": "08/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "5,445",
          "tarjetaCredito": "2025.00 ",
          "tarjetaDebito": "4,412",
          "total": "11882.00 "
      },
      {
          "fecha": "07/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-2,344",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2344.00)"
      },
      {
          "fecha": "07/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "2,344",
          "tarjetaCredito": "6974.00 ",
          "tarjetaDebito": "3,384",
          "total": "12702.00 "
      },
      {
          "fecha": "06/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-13,421",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13421.00)"
      },
      {
          "fecha": "06/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "13,421",
          "tarjetaCredito": "15298.40 ",
          "tarjetaDebito": "0",
          "total": "28719.40 "
      },
      {
          "fecha": "05/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,397",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5397.00)"
      },
      {
          "fecha": "05/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "21.00 ",
          "efectivo": "5,397",
          "tarjetaCredito": "8388.65 ",
          "tarjetaDebito": "13,615",
          "total": "27400.65 "
      },
      {
          "fecha": "04/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "6,410",
          "tarjetaCredito": "4637.00 ",
          "tarjetaDebito": "4,494",
          "total": "15541.00 "
      },
      {
          "fecha": "04/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-6,410",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6410.00)"
      },
      {
          "fecha": "03/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,700",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2700.00)"
      },
      {
          "fecha": "03/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "2,700",
          "tarjetaCredito": "317.00 ",
          "tarjetaDebito": "6,106",
          "total": "9123.00 "
      },
      {
          "fecha": "02/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "5,087",
          "tarjetaCredito": "5024.00 ",
          "tarjetaDebito": "4,258",
          "total": "14369.00 "
      },
      {
          "fecha": "02/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-5,087",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5087.00)"
      },
      {
          "fecha": "01/02/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,863.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4863.50)"
      },
      {
          "fecha": "01/02/2021",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "4,864",
          "tarjetaCredito": "3740.50 ",
          "tarjetaDebito": "0",
          "total": "8604.50 "
      },
      {
          "fecha": "31/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,154",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5154.00)"
      },
      {
          "fecha": "31/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "23.00 ",
          "efectivo": "5,154",
          "tarjetaCredito": "5322.00 ",
          "tarjetaDebito": "10,230",
          "total": "20706.00 "
      },
      {
          "fecha": "30/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "8,010",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "6,319",
          "total": "14329.00 "
      },
      {
          "fecha": "30/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-8,010",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8010.00)"
      },
      {
          "fecha": "29/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "5,362",
          "tarjetaCredito": "2716.00 ",
          "tarjetaDebito": "10,001.6",
          "total": "18079.60 "
      },
      {
          "fecha": "29/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,362",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5362.00)"
      },
      {
          "fecha": "28/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-4,383",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4383.00)"
      },
      {
          "fecha": "28/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "4,383",
          "tarjetaCredito": "2370.00 ",
          "tarjetaDebito": "4,547",
          "total": "11300.00 "
      },
      {
          "fecha": "27/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "10,564",
          "tarjetaCredito": "2458.00 ",
          "tarjetaDebito": "2,970",
          "total": "15992.00 "
      },
      {
          "fecha": "27/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-10,564",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10564.00)"
      },
      {
          "fecha": "26/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "1,250",
          "tarjetaCredito": "7201.00 ",
          "tarjetaDebito": "3,829",
          "total": "12280.00 "
      },
      {
          "fecha": "26/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,250",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1250.00)"
      },
      {
          "fecha": "25/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "3,990",
          "tarjetaCredito": "3896.00 ",
          "tarjetaDebito": "515",
          "total": "8401.00 "
      },
      {
          "fecha": "25/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,990",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3990.00)"
      },
      {
          "fecha": "24/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,480",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6480.00)"
      },
      {
          "fecha": "24/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "26.00 ",
          "efectivo": "6,480",
          "tarjetaCredito": "9941.00 ",
          "tarjetaDebito": "12,156",
          "total": "28577.00 "
      },
      {
          "fecha": "23/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "3,292",
          "tarjetaCredito": "5593.10 ",
          "tarjetaDebito": "3,485",
          "total": "12370.10 "
      },
      {
          "fecha": "23/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,292",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3292.00)"
      },
      {
          "fecha": "22/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "11,854",
          "tarjetaCredito": "6518.00 ",
          "tarjetaDebito": "5,346",
          "total": "23718.00 "
      },
      {
          "fecha": "22/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-11,854",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11854.00)"
      },
      {
          "fecha": "21/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-2,320",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2320.00)"
      },
      {
          "fecha": "21/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "2,320",
          "tarjetaCredito": "5086.00 ",
          "tarjetaDebito": "2,251",
          "total": "9657.00 "
      },
      {
          "fecha": "20/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-14,587",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(14587.00)"
      },
      {
          "fecha": "20/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "14,587",
          "tarjetaCredito": "1873.00 ",
          "tarjetaDebito": "3,620",
          "total": "20080.00 "
      },
      {
          "fecha": "19/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,941",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3941.00)"
      },
      {
          "fecha": "19/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "3,941",
          "tarjetaCredito": "6189.14 ",
          "tarjetaDebito": "11,033",
          "total": "21163.14 "
      },
      {
          "fecha": "18/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "2,546",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,339",
          "total": "3885.00 "
      },
      {
          "fecha": "18/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,546",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2546.00)"
      },
      {
          "fecha": "17/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-11,306",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11306.00)"
      },
      {
          "fecha": "17/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "11,306",
          "tarjetaCredito": "5407.00 ",
          "tarjetaDebito": "1,654",
          "total": "18367.00 "
      },
      {
          "fecha": "16/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-2,581",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2581.00)"
      },
      {
          "fecha": "16/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "2,581",
          "tarjetaCredito": "12846.15 ",
          "tarjetaDebito": "1,110",
          "total": "16537.15 "
      },
      {
          "fecha": "15/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "23.00 ",
          "efectivo": "12,883",
          "tarjetaCredito": "2855.00 ",
          "tarjetaDebito": "16,777",
          "total": "32515.00 "
      },
      {
          "fecha": "15/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-12,883",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(12883.00)"
      },
      {
          "fecha": "14/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,950",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3950.00)"
      },
      {
          "fecha": "14/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "3,950",
          "tarjetaCredito": "3239.00 ",
          "tarjetaDebito": "4,779",
          "total": "11968.00 "
      },
      {
          "fecha": "13/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-6,996",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6996.00)"
      },
      {
          "fecha": "13/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "6,996",
          "tarjetaCredito": "4033.00 ",
          "tarjetaDebito": "4,263",
          "total": "15292.00 "
      },
      {
          "fecha": "12/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,459",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5459.00)"
      },
      {
          "fecha": "12/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "5,459",
          "tarjetaCredito": "2253.00 ",
          "tarjetaDebito": "0",
          "total": "7712.00 "
      },
      {
          "fecha": "11/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "7,419",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,717",
          "total": "10136.00 "
      },
      {
          "fecha": "11/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-7,419",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7419.00)"
      },
      {
          "fecha": "10/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "4,560",
          "tarjetaCredito": "2343.00 ",
          "tarjetaDebito": "5,689",
          "total": "12592.00 "
      },
      {
          "fecha": "10/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-4,560",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4560.00)"
      },
      {
          "fecha": "09/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "4,458",
          "tarjetaCredito": "3829.00 ",
          "tarjetaDebito": "5,753",
          "total": "14040.00 "
      },
      {
          "fecha": "09/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-4,458",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4458.00)"
      },
      {
          "fecha": "08/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-11,219",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11219.00)"
      },
      {
          "fecha": "08/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "11,219",
          "tarjetaCredito": "2284.00 ",
          "tarjetaDebito": "659",
          "total": "14162.00 "
      },
      {
          "fecha": "07/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "1,318",
          "tarjetaCredito": "1855.00 ",
          "tarjetaDebito": "3,649",
          "total": "6822.00 "
      },
      {
          "fecha": "07/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,318",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1318.00)"
      },
      {
          "fecha": "06/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "4,369",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,959",
          "total": "6328.00 "
      },
      {
          "fecha": "06/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,369",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4369.00)"
      },
      {
          "fecha": "04/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-700",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(700.00)"
      },
      {
          "fecha": "04/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "700",
          "tarjetaCredito": "750.00 ",
          "tarjetaDebito": "7,603",
          "total": "9053.00 "
      },
      {
          "fecha": "03/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,250",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1250.00)"
      },
      {
          "fecha": "03/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "1,250",
          "tarjetaCredito": "4536.00 ",
          "tarjetaDebito": "5,572",
          "total": "11358.00 "
      },
      {
          "fecha": "02/01/2021",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "2,552",
          "tarjetaCredito": "1545.00 ",
          "tarjetaDebito": "754",
          "total": "4851.00 "
      },
      {
          "fecha": "02/01/2021",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,552",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2552.00)"
      },
      {
          "fecha": "31/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-29,031",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(29031.00)"
      },
      {
          "fecha": "31/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "39.00 ",
          "efectivo": "29,031.8",
          "tarjetaCredito": "4703.00 ",
          "tarjetaDebito": "7,194",
          "total": "40928.80 "
      },
      {
          "fecha": "30/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "5,586",
          "tarjetaCredito": "3381.00 ",
          "tarjetaDebito": "4,819",
          "total": "13786.00 "
      },
      {
          "fecha": "30/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-5,586",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5586.00)"
      },
      {
          "fecha": "29/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "4,136",
          "tarjetaCredito": "2777.00 ",
          "tarjetaDebito": "197",
          "total": "7110.00 "
      },
      {
          "fecha": "29/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,136",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4136.00)"
      },
      {
          "fecha": "28/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,935",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2935.00)"
      },
      {
          "fecha": "28/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "2,935",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,712",
          "total": "5647.00 "
      },
      {
          "fecha": "27/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-800",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(800.00)"
      },
      {
          "fecha": "27/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "800",
          "tarjetaCredito": "629.00 ",
          "tarjetaDebito": "3,427",
          "total": "4856.00 "
      },
      {
          "fecha": "26/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "986",
          "tarjetaCredito": "10255.70 ",
          "tarjetaDebito": "670",
          "total": "11911.70 "
      },
      {
          "fecha": "26/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-986",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(986.00)"
      },
      {
          "fecha": "24/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "7.00 ",
          "efectivo": "-24,261",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(24261.00)"
      },
      {
          "fecha": "24/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "32.00 ",
          "efectivo": "24,261",
          "tarjetaCredito": "8064.00 ",
          "tarjetaDebito": "4,636",
          "total": "36961.00 "
      },
      {
          "fecha": "23/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "17,391",
          "tarjetaCredito": "4370.00 ",
          "tarjetaDebito": "1,154",
          "total": "22915.00 "
      },
      {
          "fecha": "23/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-17,391",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(17391.00)"
      },
      {
          "fecha": "22/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "5,818",
          "tarjetaCredito": "6012.00 ",
          "tarjetaDebito": "1,127",
          "total": "12957.00 "
      },
      {
          "fecha": "22/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,818",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5818.00)"
      },
      {
          "fecha": "21/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-9,592",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9592.00)"
      },
      {
          "fecha": "21/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "9,592",
          "tarjetaCredito": "523.00 ",
          "tarjetaDebito": "4,624",
          "total": "14739.00 "
      },
      {
          "fecha": "20/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "6,236",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,163",
          "total": "10399.00 "
      },
      {
          "fecha": "20/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-6,236",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6236.00)"
      },
      {
          "fecha": "19/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-4,884",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4884.00)"
      },
      {
          "fecha": "19/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "4,884",
          "tarjetaCredito": "1292.00 ",
          "tarjetaDebito": "1,980",
          "total": "8156.00 "
      },
      {
          "fecha": "18/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-17,630",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(17630.00)"
      },
      {
          "fecha": "18/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "17,630",
          "tarjetaCredito": "737.00 ",
          "tarjetaDebito": "12,981",
          "total": "31348.00 "
      },
      {
          "fecha": "17/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "8,078",
          "tarjetaCredito": "3994.45 ",
          "tarjetaDebito": "8,528",
          "total": "20600.45 "
      },
      {
          "fecha": "17/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-8,078",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8078.00)"
      },
      {
          "fecha": "16/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "10,750",
          "tarjetaCredito": "7524.00 ",
          "tarjetaDebito": "4,408",
          "total": "22682.00 "
      },
      {
          "fecha": "16/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-10,750",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10750.00)"
      },
      {
          "fecha": "15/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "6,820",
          "tarjetaCredito": "19668.70 ",
          "tarjetaDebito": "10,706",
          "total": "37194.70 "
      },
      {
          "fecha": "15/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,820",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6820.00)"
      },
      {
          "fecha": "14/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,648",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4648.00)"
      },
      {
          "fecha": "14/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "4,648",
          "tarjetaCredito": "1146.00 ",
          "tarjetaDebito": "9,352",
          "total": "15146.00 "
      },
      {
          "fecha": "13/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "11,208",
          "tarjetaCredito": "5183.00 ",
          "tarjetaDebito": "11,149",
          "total": "27540.00 "
      },
      {
          "fecha": "13/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-11,208",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11208.00)"
      },
      {
          "fecha": "12/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "3,066",
          "tarjetaCredito": "8821.00 ",
          "tarjetaDebito": "11,649",
          "total": "23536.00 "
      },
      {
          "fecha": "12/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-6,165",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6165.00)"
      },
      {
          "fecha": "11/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "38.00 ",
          "efectivo": "9,639",
          "tarjetaCredito": "8386.00 ",
          "tarjetaDebito": "41,049",
          "total": "59074.00 "
      },
      {
          "fecha": "11/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-6,540",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6540.00)"
      },
      {
          "fecha": "10/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "7,381",
          "tarjetaCredito": "3985.85 ",
          "tarjetaDebito": "2,026",
          "total": "13392.85 "
      },
      {
          "fecha": "10/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-7,381",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7381.00)"
      },
      {
          "fecha": "09/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "3,630",
          "tarjetaCredito": "3942.00 ",
          "tarjetaDebito": "2,869",
          "total": "10441.00 "
      },
      {
          "fecha": "09/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-3,630",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3630.00)"
      },
      {
          "fecha": "08/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,606",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4606.00)"
      },
      {
          "fecha": "08/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "4,606",
          "tarjetaCredito": "2369.00 ",
          "tarjetaDebito": "7,511",
          "total": "14486.00 "
      },
      {
          "fecha": "07/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-11,721",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11721.00)"
      },
      {
          "fecha": "07/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "11,721",
          "tarjetaCredito": "2818.00 ",
          "tarjetaDebito": "6,540",
          "total": "21079.00 "
      },
      {
          "fecha": "06/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "4,093",
          "tarjetaCredito": "5430.00 ",
          "tarjetaDebito": "2,457",
          "total": "11980.00 "
      },
      {
          "fecha": "06/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,093",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4093.00)"
      },
      {
          "fecha": "05/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "25.00 ",
          "efectivo": "4,414",
          "tarjetaCredito": "15861.00 ",
          "tarjetaDebito": "27,569",
          "total": "47844.00 "
      },
      {
          "fecha": "05/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-7,288",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7288.00)"
      },
      {
          "fecha": "04/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "34.00 ",
          "efectivo": "17,266",
          "tarjetaCredito": "13312.80 ",
          "tarjetaDebito": "11,931",
          "total": "42509.80 "
      },
      {
          "fecha": "04/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-14,392",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(14392.00)"
      },
      {
          "fecha": "03/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-16,819",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(16819.00)"
      },
      {
          "fecha": "03/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "16,819",
          "tarjetaCredito": "4512.00 ",
          "tarjetaDebito": "8,268",
          "total": "29599.00 "
      },
      {
          "fecha": "02/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-12,204",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(12204.00)"
      },
      {
          "fecha": "02/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "12,204",
          "tarjetaCredito": "9417.00 ",
          "tarjetaDebito": "8,083",
          "total": "29704.00 "
      },
      {
          "fecha": "01/12/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "7,205",
          "tarjetaCredito": "1389.00 ",
          "tarjetaDebito": "7,297",
          "total": "15891.00 "
      },
      {
          "fecha": "01/12/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-7,205",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7205.00)"
      },
      {
          "fecha": "30/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "6,514",
          "tarjetaCredito": "1914.00 ",
          "tarjetaDebito": "10,745",
          "total": "19173.00 "
      },
      {
          "fecha": "30/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-6,513.4",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6513.40)"
      },
      {
          "fecha": "29/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-13,040",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13040.00)"
      },
      {
          "fecha": "29/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "13,040",
          "tarjetaCredito": "4982.00 ",
          "tarjetaDebito": "11,276",
          "total": "29298.00 "
      },
      {
          "fecha": "28/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "1,020",
          "tarjetaCredito": "5990.00 ",
          "tarjetaDebito": "16,500",
          "total": "23510.00 "
      },
      {
          "fecha": "28/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,020",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1020.00)"
      },
      {
          "fecha": "27/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,148",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8148.00)"
      },
      {
          "fecha": "27/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "30.00 ",
          "efectivo": "8,148",
          "tarjetaCredito": "31554.45 ",
          "tarjetaDebito": "10,910",
          "total": "50612.45 "
      },
      {
          "fecha": "26/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-5,070",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5070.00)"
      },
      {
          "fecha": "26/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "5,070",
          "tarjetaCredito": "2659.00 ",
          "tarjetaDebito": "9,858",
          "total": "17587.00 "
      },
      {
          "fecha": "25/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "9,883",
          "tarjetaCredito": "6125.50 ",
          "tarjetaDebito": "4,420",
          "total": "20428.50 "
      },
      {
          "fecha": "25/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-9,883",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9883.00)"
      },
      {
          "fecha": "24/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-9,951",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9951.00)"
      },
      {
          "fecha": "24/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "11,257",
          "tarjetaCredito": "3198.20 ",
          "tarjetaDebito": "3,327",
          "total": "17782.20 "
      },
      {
          "fecha": "23/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "7,271",
          "tarjetaCredito": "4261.00 ",
          "tarjetaDebito": "3,940",
          "total": "15472.00 "
      },
      {
          "fecha": "23/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-7,271",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7271.00)"
      },
      {
          "fecha": "22/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,995",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2995.00)"
      },
      {
          "fecha": "22/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "2,995",
          "tarjetaCredito": "1992.00 ",
          "tarjetaDebito": "10,188",
          "total": "15175.00 "
      },
      {
          "fecha": "21/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-6,330",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6330.00)"
      },
      {
          "fecha": "21/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "6,330",
          "tarjetaCredito": "12324.00 ",
          "tarjetaDebito": "18,840",
          "total": "37494.00 "
      },
      {
          "fecha": "20/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-6,742",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6742.00)"
      },
      {
          "fecha": "20/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "6,742",
          "tarjetaCredito": "2941.80 ",
          "tarjetaDebito": "8,148",
          "total": "17831.80 "
      },
      {
          "fecha": "19/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-10,082",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10082.00)"
      },
      {
          "fecha": "19/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "10,082",
          "tarjetaCredito": "4473.70 ",
          "tarjetaDebito": "12,967",
          "total": "27522.70 "
      },
      {
          "fecha": "18/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "5,400",
          "tarjetaCredito": "5355.00 ",
          "tarjetaDebito": "5,388",
          "total": "16143.00 "
      },
      {
          "fecha": "18/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5400.00)"
      },
      {
          "fecha": "17/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "6,476",
          "tarjetaCredito": "7997.40 ",
          "tarjetaDebito": "1,910",
          "total": "16383.40 "
      },
      {
          "fecha": "17/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-6,476",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6476.00)"
      },
      {
          "fecha": "16/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "5,549",
          "tarjetaCredito": "8554.00 ",
          "tarjetaDebito": "15,847",
          "total": "29950.00 "
      },
      {
          "fecha": "16/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-5,549",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5549.00)"
      },
      {
          "fecha": "15/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "8,716",
          "tarjetaCredito": "6966.40 ",
          "tarjetaDebito": "6,255",
          "total": "21937.40 "
      },
      {
          "fecha": "15/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,716",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8716.00)"
      },
      {
          "fecha": "14/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-11,398",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11398.00)"
      },
      {
          "fecha": "14/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "1,865",
          "tarjetaCredito": "11178.00 ",
          "tarjetaDebito": "19,535",
          "total": "32578.00 "
      },
      {
          "fecha": "13/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "28.00 ",
          "efectivo": "10,821",
          "tarjetaCredito": "14894.70 ",
          "tarjetaDebito": "18,077",
          "total": "43792.70 "
      },
      {
          "fecha": "13/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,288",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1288.00)"
      },
      {
          "fecha": "12/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-8,429",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8429.00)"
      },
      {
          "fecha": "12/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "21.00 ",
          "efectivo": "8,429",
          "tarjetaCredito": "4191.00 ",
          "tarjetaDebito": "6,033",
          "total": "18653.00 "
      },
      {
          "fecha": "11/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "5,950",
          "tarjetaCredito": "3223.50 ",
          "tarjetaDebito": "4,043.5",
          "total": "13217.00 "
      },
      {
          "fecha": "11/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-5,950",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5950.00)"
      },
      {
          "fecha": "10/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-10,060",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10060.00)"
      },
      {
          "fecha": "10/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "10,060",
          "tarjetaCredito": "1328.00 ",
          "tarjetaDebito": "4,690",
          "total": "16078.00 "
      },
      {
          "fecha": "09/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-6,937",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6937.00)"
      },
      {
          "fecha": "09/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "6,937",
          "tarjetaCredito": "1700.00 ",
          "tarjetaDebito": "8,917",
          "total": "17554.00 "
      },
      {
          "fecha": "08/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,605",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2605.00)"
      },
      {
          "fecha": "08/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "2,605",
          "tarjetaCredito": "3028.50 ",
          "tarjetaDebito": "13,131.5",
          "total": "18765.00 "
      },
      {
          "fecha": "07/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "10,940",
          "tarjetaCredito": "5564.30 ",
          "tarjetaDebito": "9,819",
          "total": "26323.30 "
      },
      {
          "fecha": "07/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-10,940",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10940.00)"
      },
      {
          "fecha": "06/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "28.00 ",
          "efectivo": "9,392",
          "tarjetaCredito": "22817.70 ",
          "tarjetaDebito": "9,550",
          "total": "41759.70 "
      },
      {
          "fecha": "06/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-9,392",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9392.00)"
      },
      {
          "fecha": "05/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,803",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5803.00)"
      },
      {
          "fecha": "05/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "5,803",
          "tarjetaCredito": "5767.00 ",
          "tarjetaDebito": "22,701",
          "total": "34271.00 "
      },
      {
          "fecha": "04/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "13,720",
          "tarjetaCredito": "3162.85 ",
          "tarjetaDebito": "5,849",
          "total": "22731.85 "
      },
      {
          "fecha": "04/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-13,720",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13720.00)"
      },
      {
          "fecha": "03/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "13,695",
          "tarjetaCredito": "2709.00 ",
          "tarjetaDebito": "4,054",
          "total": "20458.00 "
      },
      {
          "fecha": "03/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-13,695",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13695.00)"
      },
      {
          "fecha": "02/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "6,352",
          "tarjetaCredito": "6873.00 ",
          "tarjetaDebito": "5,052",
          "total": "18277.00 "
      },
      {
          "fecha": "02/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,352",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6352.00)"
      },
      {
          "fecha": "01/11/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-7,241",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7241.00)"
      },
      {
          "fecha": "01/11/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "7,241",
          "tarjetaCredito": "2672.00 ",
          "tarjetaDebito": "2,714",
          "total": "12627.00 "
      },
      {
          "fecha": "31/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "23.00 ",
          "efectivo": "8,187",
          "tarjetaCredito": "14773.00 ",
          "tarjetaDebito": "9,843",
          "total": "32803.00 "
      },
      {
          "fecha": "31/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-8,187",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8187.00)"
      },
      {
          "fecha": "30/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "4,650",
          "tarjetaCredito": "8315.75 ",
          "tarjetaDebito": "8,839",
          "total": "21804.75 "
      },
      {
          "fecha": "30/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,650",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4650.00)"
      },
      {
          "fecha": "29/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-9,573",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9573.00)"
      },
      {
          "fecha": "29/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "25.00 ",
          "efectivo": "9,573",
          "tarjetaCredito": "5513.00 ",
          "tarjetaDebito": "15,679",
          "total": "30765.00 "
      },
      {
          "fecha": "28/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,743",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5743.00)"
      },
      {
          "fecha": "28/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "5,743",
          "tarjetaCredito": "19451.20 ",
          "tarjetaDebito": "5,134",
          "total": "30328.20 "
      },
      {
          "fecha": "27/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "10,935",
          "tarjetaCredito": "19739.50 ",
          "tarjetaDebito": "7,898",
          "total": "38572.50 "
      },
      {
          "fecha": "27/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-10,935",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10935.00)"
      },
      {
          "fecha": "26/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,061",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8061.00)"
      },
      {
          "fecha": "26/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "8,061",
          "tarjetaCredito": "4354.00 ",
          "tarjetaDebito": "1,884",
          "total": "14299.00 "
      },
      {
          "fecha": "25/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,897",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4897.00)"
      },
      {
          "fecha": "25/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "4,897",
          "tarjetaCredito": "7575.00 ",
          "tarjetaDebito": "12,603",
          "total": "25075.00 "
      },
      {
          "fecha": "24/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "31.00 ",
          "efectivo": "4,036",
          "tarjetaCredito": "35911.00 ",
          "tarjetaDebito": "16,835",
          "total": "56782.00 "
      },
      {
          "fecha": "24/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,036",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4036.00)"
      },
      {
          "fecha": "23/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-14,095",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(14095.00)"
      },
      {
          "fecha": "23/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "28.00 ",
          "efectivo": "14,095",
          "tarjetaCredito": "7016.00 ",
          "tarjetaDebito": "22,804.4",
          "total": "43915.40 "
      },
      {
          "fecha": "22/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "5,764",
          "tarjetaCredito": "5286.00 ",
          "tarjetaDebito": "5,937",
          "total": "16987.00 "
      },
      {
          "fecha": "22/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,764",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5764.00)"
      },
      {
          "fecha": "21/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,480",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2480.00)"
      },
      {
          "fecha": "21/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "2,480",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,225",
          "total": "7705.00 "
      },
      {
          "fecha": "20/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "14,352",
          "tarjetaCredito": "3814.00 ",
          "tarjetaDebito": "6,358",
          "total": "24524.00 "
      },
      {
          "fecha": "20/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-14,352",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(14352.00)"
      },
      {
          "fecha": "19/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,631",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2631.00)"
      },
      {
          "fecha": "19/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "2,631",
          "tarjetaCredito": "7615.00 ",
          "tarjetaDebito": "1,570",
          "total": "11816.00 "
      },
      {
          "fecha": "18/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-17,337",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(17337.00)"
      },
      {
          "fecha": "18/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "17,337",
          "tarjetaCredito": "4069.70 ",
          "tarjetaDebito": "6,107",
          "total": "27513.70 "
      },
      {
          "fecha": "17/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-5,641",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5641.00)"
      },
      {
          "fecha": "17/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "26.00 ",
          "efectivo": "5,641",
          "tarjetaCredito": "11012.00 ",
          "tarjetaDebito": "17,063",
          "total": "33716.00 "
      },
      {
          "fecha": "16/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "3,398",
          "tarjetaCredito": "8768.50 ",
          "tarjetaDebito": "13,581",
          "total": "25747.50 "
      },
      {
          "fecha": "16/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,398",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3398.00)"
      },
      {
          "fecha": "15/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,133",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3133.00)"
      },
      {
          "fecha": "15/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "3,133",
          "tarjetaCredito": "3010.00 ",
          "tarjetaDebito": "7,966",
          "total": "14109.00 "
      },
      {
          "fecha": "14/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,770",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5770.00)"
      },
      {
          "fecha": "14/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "5,770",
          "tarjetaCredito": "3069.00 ",
          "tarjetaDebito": "3,753",
          "total": "12592.00 "
      },
      {
          "fecha": "13/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,469",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8469.00)"
      },
      {
          "fecha": "13/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "8,469",
          "tarjetaCredito": "693.45 ",
          "tarjetaDebito": "2,880",
          "total": "12042.45 "
      },
      {
          "fecha": "12/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,949",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4949.00)"
      },
      {
          "fecha": "12/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "4,949",
          "tarjetaCredito": "1830.00 ",
          "tarjetaDebito": "4,757",
          "total": "11536.00 "
      },
      {
          "fecha": "11/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,833",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2833.00)"
      },
      {
          "fecha": "11/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "2,833",
          "tarjetaCredito": "4543.00 ",
          "tarjetaDebito": "4,726",
          "total": "12102.00 "
      },
      {
          "fecha": "10/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-23,129",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(23129.00)"
      },
      {
          "fecha": "10/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "29.00 ",
          "efectivo": "20,774",
          "tarjetaCredito": "23439.00 ",
          "tarjetaDebito": "13,000",
          "total": "57213.00 "
      },
      {
          "fecha": "09/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "2,355",
          "tarjetaCredito": "11048.85 ",
          "tarjetaDebito": "9,989",
          "total": "23392.85 "
      },
      {
          "fecha": "08/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "10,140",
          "tarjetaCredito": "6012.00 ",
          "tarjetaDebito": "4,112",
          "total": "20264.00 "
      },
      {
          "fecha": "08/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-10,140",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10140.00)"
      },
      {
          "fecha": "07/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "985",
          "tarjetaCredito": "13866.50 ",
          "tarjetaDebito": "4,701",
          "total": "19552.50 "
      },
      {
          "fecha": "07/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-985",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(985.00)"
      },
      {
          "fecha": "06/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "1,160",
          "tarjetaCredito": "4214.00 ",
          "tarjetaDebito": "9,341",
          "total": "14715.00 "
      },
      {
          "fecha": "06/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,160",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1160.00)"
      },
      {
          "fecha": "05/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "5,345",
          "tarjetaCredito": "6071.95 ",
          "tarjetaDebito": "2,949",
          "total": "14365.95 "
      },
      {
          "fecha": "05/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-5,345",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5345.00)"
      },
      {
          "fecha": "04/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "6,220",
          "tarjetaCredito": "7169.00 ",
          "tarjetaDebito": "2,595",
          "total": "15984.00 "
      },
      {
          "fecha": "04/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-12,745",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(12745.00)"
      },
      {
          "fecha": "03/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "29.00 ",
          "efectivo": "12,928",
          "tarjetaCredito": "4337.00 ",
          "tarjetaDebito": "20,351",
          "total": "37616.00 "
      },
      {
          "fecha": "03/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,403",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6403.00)"
      },
      {
          "fecha": "02/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-21,571",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(21571.00)"
      },
      {
          "fecha": "02/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "30.00 ",
          "efectivo": "21,571",
          "tarjetaCredito": "11667.00 ",
          "tarjetaDebito": "10,719",
          "total": "43957.00 "
      },
      {
          "fecha": "01/10/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "6,607",
          "tarjetaCredito": "10498.00 ",
          "tarjetaDebito": "6,078",
          "total": "23183.00 "
      },
      {
          "fecha": "01/10/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "7.00 ",
          "efectivo": "-6,607",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6607.00)"
      },
      {
          "fecha": "30/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "5,675",
          "tarjetaCredito": "7360.00 ",
          "tarjetaDebito": "6,054",
          "total": "19089.00 "
      },
      {
          "fecha": "30/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-5,675",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5675.00)"
      },
      {
          "fecha": "29/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "12,336",
          "tarjetaCredito": "3916.00 ",
          "tarjetaDebito": "10,334",
          "total": "26586.00 "
      },
      {
          "fecha": "29/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-12,336",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(12336.00)"
      },
      {
          "fecha": "28/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "3,656",
          "tarjetaCredito": "5179.00 ",
          "tarjetaDebito": "8,866",
          "total": "17701.00 "
      },
      {
          "fecha": "28/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "7.00 ",
          "efectivo": "-3,656",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3656.00)"
      },
      {
          "fecha": "27/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "6,964",
          "tarjetaCredito": "4990.00 ",
          "tarjetaDebito": "1,729",
          "total": "13683.00 "
      },
      {
          "fecha": "27/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,964",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6964.00)"
      },
      {
          "fecha": "26/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-13,736",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13736.00)"
      },
      {
          "fecha": "26/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "30.00 ",
          "efectivo": "7,585",
          "tarjetaCredito": "8807.00 ",
          "tarjetaDebito": "17,553",
          "total": "33945.00 "
      },
      {
          "fecha": "25/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "37.00 ",
          "efectivo": "12,997",
          "tarjetaCredito": "31547.50 ",
          "tarjetaDebito": "13,824.5",
          "total": "58369.00 "
      },
      {
          "fecha": "25/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-8,295",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8295.00)"
      },
      {
          "fecha": "24/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-806",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(806.00)"
      },
      {
          "fecha": "24/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "2,255",
          "tarjetaCredito": "8486.00 ",
          "tarjetaDebito": "5,993",
          "total": "16734.00 "
      },
      {
          "fecha": "23/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-5,583",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5583.00)"
      },
      {
          "fecha": "23/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "8,501",
          "tarjetaCredito": "4488.00 ",
          "tarjetaDebito": "4,291",
          "total": "17280.00 "
      },
      {
          "fecha": "22/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "8,380",
          "tarjetaCredito": "4095.00 ",
          "tarjetaDebito": "2,852",
          "total": "15327.00 "
      },
      {
          "fecha": "22/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-8,380",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8380.00)"
      },
      {
          "fecha": "21/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,723",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,745",
          "total": "4468.00 "
      },
      {
          "fecha": "21/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,723",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1723.00)"
      },
      {
          "fecha": "20/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "4,712",
          "tarjetaCredito": "2598.00 ",
          "tarjetaDebito": "8,522",
          "total": "15832.00 "
      },
      {
          "fecha": "20/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-4,712",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4712.00)"
      },
      {
          "fecha": "19/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-13,350",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13350.00)"
      },
      {
          "fecha": "19/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "29.00 ",
          "efectivo": "13,350",
          "tarjetaCredito": "9622.00 ",
          "tarjetaDebito": "17,142",
          "total": "40114.00 "
      },
      {
          "fecha": "18/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "3,164",
          "tarjetaCredito": "2535.50 ",
          "tarjetaDebito": "6,423.5",
          "total": "12123.00 "
      },
      {
          "fecha": "18/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,164",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3164.00)"
      },
      {
          "fecha": "17/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,325",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5325.00)"
      },
      {
          "fecha": "17/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "5,325",
          "tarjetaCredito": "13416.00 ",
          "tarjetaDebito": "11,696",
          "total": "30437.00 "
      },
      {
          "fecha": "16/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "909",
          "tarjetaCredito": "3603.00 ",
          "tarjetaDebito": "4,559",
          "total": "9071.00 "
      },
      {
          "fecha": "16/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-909",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(909.00)"
      },
      {
          "fecha": "15/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "265",
          "tarjetaCredito": "1998.00 ",
          "tarjetaDebito": "4,861",
          "total": "7124.00 "
      },
      {
          "fecha": "15/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-265",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(265.00)"
      },
      {
          "fecha": "14/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "6,492",
          "tarjetaCredito": "4022.00 ",
          "tarjetaDebito": "2,656",
          "total": "13170.00 "
      },
      {
          "fecha": "14/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-6,492",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6492.00)"
      },
      {
          "fecha": "13/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "5,330",
          "tarjetaCredito": "3270.90 ",
          "tarjetaDebito": "3,044",
          "total": "11644.90 "
      },
      {
          "fecha": "13/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,330",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5330.00)"
      },
      {
          "fecha": "12/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "34.00 ",
          "efectivo": "17,490",
          "tarjetaCredito": "19763.00 ",
          "tarjetaDebito": "18,333",
          "total": "55586.00 "
      },
      {
          "fecha": "12/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-17,490",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(17490.00)"
      },
      {
          "fecha": "11/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-8,005",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8005.00)"
      },
      {
          "fecha": "11/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "8,005",
          "tarjetaCredito": "3537.00 ",
          "tarjetaDebito": "12,731",
          "total": "24273.00 "
      },
      {
          "fecha": "10/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "4,895",
          "tarjetaCredito": "16984.00 ",
          "tarjetaDebito": "2,267",
          "total": "24146.00 "
      },
      {
          "fecha": "10/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,895",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4895.00)"
      },
      {
          "fecha": "09/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "4,244",
          "tarjetaCredito": "3474.00 ",
          "tarjetaDebito": "5,492",
          "total": "13210.00 "
      },
      {
          "fecha": "09/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,244",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4244.00)"
      },
      {
          "fecha": "08/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,905",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5905.00)"
      },
      {
          "fecha": "08/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "5,905",
          "tarjetaCredito": "7768.55 ",
          "tarjetaDebito": "3,354",
          "total": "17027.55 "
      },
      {
          "fecha": "07/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "8,873",
          "tarjetaCredito": "1217.70 ",
          "tarjetaDebito": "3,307",
          "total": "13397.70 "
      },
      {
          "fecha": "07/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-8,873",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8873.00)"
      },
      {
          "fecha": "06/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "7,100",
          "tarjetaCredito": "4278.00 ",
          "tarjetaDebito": "10,264",
          "total": "21642.00 "
      },
      {
          "fecha": "06/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-7,100",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7100.00)"
      },
      {
          "fecha": "05/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "29.00 ",
          "efectivo": "6,726",
          "tarjetaCredito": "11336.60 ",
          "tarjetaDebito": "16,303",
          "total": "34365.60 "
      },
      {
          "fecha": "05/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,726",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6726.00)"
      },
      {
          "fecha": "04/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "31.00 ",
          "efectivo": "11,109",
          "tarjetaCredito": "14318.00 ",
          "tarjetaDebito": "17,093",
          "total": "42520.00 "
      },
      {
          "fecha": "04/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-11,097",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11097.00)"
      },
      {
          "fecha": "03/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-3,777",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3777.00)"
      },
      {
          "fecha": "03/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "3,777",
          "tarjetaCredito": "6610.00 ",
          "tarjetaDebito": "4,411",
          "total": "14798.00 "
      },
      {
          "fecha": "02/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-9,981",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9981.00)"
      },
      {
          "fecha": "02/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "9,981",
          "tarjetaCredito": "1224.00 ",
          "tarjetaDebito": "12,619",
          "total": "23824.00 "
      },
      {
          "fecha": "01/09/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "8,780",
          "tarjetaCredito": "3414.60 ",
          "tarjetaDebito": "4,400.45",
          "total": "16595.05 "
      },
      {
          "fecha": "01/09/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,780",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8780.00)"
      },
      {
          "fecha": "31/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "3,650",
          "tarjetaCredito": "1691.00 ",
          "tarjetaDebito": "3,426",
          "total": "8767.00 "
      },
      {
          "fecha": "31/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-3,115",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3115.00)"
      },
      {
          "fecha": "30/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "6,754",
          "tarjetaCredito": "2901.00 ",
          "tarjetaDebito": "13,747",
          "total": "23402.00 "
      },
      {
          "fecha": "30/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-6,754",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6754.00)"
      },
      {
          "fecha": "29/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-15,063",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(15063.00)"
      },
      {
          "fecha": "29/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "29.00 ",
          "efectivo": "14,256",
          "tarjetaCredito": "18695.00 ",
          "tarjetaDebito": "17,850",
          "total": "50801.00 "
      },
      {
          "fecha": "28/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "3,647",
          "tarjetaCredito": "18244.00 ",
          "tarjetaDebito": "10,818",
          "total": "32709.00 "
      },
      {
          "fecha": "27/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "3,375",
          "tarjetaCredito": "8476.80 ",
          "tarjetaDebito": "5,177",
          "total": "17028.80 "
      },
      {
          "fecha": "27/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-3,375",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3375.00)"
      },
      {
          "fecha": "26/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,113",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3113.00)"
      },
      {
          "fecha": "26/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "3,113",
          "tarjetaCredito": "5845.00 ",
          "tarjetaDebito": "7,060",
          "total": "16018.00 "
      },
      {
          "fecha": "25/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "4,085",
          "tarjetaCredito": "2149.00 ",
          "tarjetaDebito": "0",
          "total": "6234.00 "
      },
      {
          "fecha": "25/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-4,085",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4085.00)"
      },
      {
          "fecha": "24/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,180",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2180.00)"
      },
      {
          "fecha": "24/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "2,180",
          "tarjetaCredito": "1216.00 ",
          "tarjetaDebito": "1,614",
          "total": "5010.00 "
      },
      {
          "fecha": "23/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "21.00 ",
          "efectivo": "7,631",
          "tarjetaCredito": "4675.00 ",
          "tarjetaDebito": "15,533",
          "total": "27839.00 "
      },
      {
          "fecha": "23/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-7,631",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7631.00)"
      },
      {
          "fecha": "22/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "1,631",
          "tarjetaCredito": "7168.00 ",
          "tarjetaDebito": "11,415",
          "total": "20214.00 "
      },
      {
          "fecha": "22/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,631",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1631.00)"
      },
      {
          "fecha": "21/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "21.00 ",
          "efectivo": "8,370",
          "tarjetaCredito": "5665.70 ",
          "tarjetaDebito": "13,605",
          "total": "27640.70 "
      },
      {
          "fecha": "21/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,982",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5982.00)"
      },
      {
          "fecha": "20/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,824",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6824.00)"
      },
      {
          "fecha": "20/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "6,824",
          "tarjetaCredito": "7389.15 ",
          "tarjetaDebito": "5,738",
          "total": "19951.15 "
      },
      {
          "fecha": "19/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "4,194",
          "tarjetaCredito": "3902.00 ",
          "tarjetaDebito": "12,893",
          "total": "20989.00 "
      },
      {
          "fecha": "19/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,194",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4194.00)"
      },
      {
          "fecha": "18/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "5,245",
          "tarjetaCredito": "8828.00 ",
          "tarjetaDebito": "2,776",
          "total": "16849.00 "
      },
      {
          "fecha": "18/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,245",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5245.00)"
      },
      {
          "fecha": "17/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "3,617",
          "tarjetaCredito": "2747.00 ",
          "tarjetaDebito": "5,997",
          "total": "12361.00 "
      },
      {
          "fecha": "17/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,617",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3617.00)"
      },
      {
          "fecha": "16/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-13,727",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(13727.00)"
      },
      {
          "fecha": "16/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "32.00 ",
          "efectivo": "13,727",
          "tarjetaCredito": "8561.40 ",
          "tarjetaDebito": "12,151",
          "total": "34439.40 "
      },
      {
          "fecha": "15/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "6,595.8",
          "tarjetaCredito": "6806.00 ",
          "tarjetaDebito": "14,550.2",
          "total": "27952.00 "
      },
      {
          "fecha": "15/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,595.8",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6595.80)"
      },
      {
          "fecha": "14/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,876",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8876.00)"
      },
      {
          "fecha": "14/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "8,876",
          "tarjetaCredito": "11669.90 ",
          "tarjetaDebito": "4,685",
          "total": "25230.90 "
      },
      {
          "fecha": "13/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,000",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1000.00)"
      },
      {
          "fecha": "13/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "1,000",
          "tarjetaCredito": "1879.45 ",
          "tarjetaDebito": "10,973",
          "total": "13852.45 "
      },
      {
          "fecha": "12/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,038",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4038.00)"
      },
      {
          "fecha": "12/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "23.00 ",
          "efectivo": "4,038",
          "tarjetaCredito": "11064.95 ",
          "tarjetaDebito": "8,261",
          "total": "23363.95 "
      },
      {
          "fecha": "11/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "5,106",
          "tarjetaCredito": "271.70 ",
          "tarjetaDebito": "6,470",
          "total": "11847.70 "
      },
      {
          "fecha": "11/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-5,106",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5106.00)"
      },
      {
          "fecha": "10/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "3,970",
          "tarjetaCredito": "1461.00 ",
          "tarjetaDebito": "1,164",
          "total": "6595.00 "
      },
      {
          "fecha": "10/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-3,970",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3970.00)"
      },
      {
          "fecha": "09/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,089",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5089.00)"
      },
      {
          "fecha": "09/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "5,089",
          "tarjetaCredito": "11356.00 ",
          "tarjetaDebito": "2,616",
          "total": "19061.00 "
      },
      {
          "fecha": "08/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "5,920",
          "tarjetaCredito": "10706.00 ",
          "tarjetaDebito": "12,037",
          "total": "28663.00 "
      },
      {
          "fecha": "08/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-5,920",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5920.00)"
      },
      {
          "fecha": "07/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "21.00 ",
          "efectivo": "4,920",
          "tarjetaCredito": "9194.00 ",
          "tarjetaDebito": "15,885",
          "total": "29999.00 "
      },
      {
          "fecha": "07/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,520",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4520.00)"
      },
      {
          "fecha": "06/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,025",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3025.00)"
      },
      {
          "fecha": "06/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "3,025",
          "tarjetaCredito": "9720.00 ",
          "tarjetaDebito": "3,216",
          "total": "15961.00 "
      },
      {
          "fecha": "05/08/2020",
          "concepto": "ENTRADA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "2,752",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "2752.00 "
      },
      {
          "fecha": "05/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "4,202",
          "tarjetaCredito": "12947.00 ",
          "tarjetaDebito": "6,927",
          "total": "24076.00 "
      },
      {
          "fecha": "05/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-6,954",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6954.00)"
      },
      {
          "fecha": "04/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "7,047",
          "tarjetaCredito": "3433.90 ",
          "tarjetaDebito": "5,012.62",
          "total": "15493.52 "
      },
      {
          "fecha": "04/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-7,047",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7047.00)"
      },
      {
          "fecha": "03/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-840",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(840.00)"
      },
      {
          "fecha": "03/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "4,966",
          "tarjetaCredito": "687.00 ",
          "tarjetaDebito": "971",
          "total": "6624.00 "
      },
      {
          "fecha": "02/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "8,109",
          "tarjetaCredito": "3156.00 ",
          "tarjetaDebito": "4,501",
          "total": "15766.00 "
      },
      {
          "fecha": "02/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-7,285",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(7285.00)"
      },
      {
          "fecha": "01/08/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-8,056",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(8056.00)"
      },
      {
          "fecha": "01/08/2020",
          "concepto": "VENTA",
          "totalTransacciones": "22.00 ",
          "efectivo": "9,647",
          "tarjetaCredito": "8238.00 ",
          "tarjetaDebito": "12,069",
          "total": "29954.00 "
      },
      {
          "fecha": "31/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-4,974",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4974.00)"
      },
      {
          "fecha": "31/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "9,984",
          "tarjetaCredito": "6079.00 ",
          "tarjetaDebito": "11,187",
          "total": "27250.00 "
      },
      {
          "fecha": "30/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "5,855",
          "tarjetaCredito": "6009.90 ",
          "tarjetaDebito": "5,965",
          "total": "17829.90 "
      },
      {
          "fecha": "30/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,355.07",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1355.07)"
      },
      {
          "fecha": "29/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "7,693.55",
          "tarjetaCredito": "5090.00 ",
          "tarjetaDebito": "4,769",
          "total": "17552.55 "
      },
      {
          "fecha": "29/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,076.05",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2076.05)"
      },
      {
          "fecha": "28/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,721",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1721.00)"
      },
      {
          "fecha": "28/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "6,204",
          "tarjetaCredito": "3251.00 ",
          "tarjetaDebito": "5,702",
          "total": "15157.00 "
      },
      {
          "fecha": "27/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-2,292.23",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2292.23)"
      },
      {
          "fecha": "27/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "4,400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,203",
          "total": "9603.00 "
      },
      {
          "fecha": "26/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,642",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2642.00)"
      },
      {
          "fecha": "26/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "5,540",
          "tarjetaCredito": "5657.00 ",
          "tarjetaDebito": "15,219",
          "total": "26416.00 "
      },
      {
          "fecha": "25/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,779.75",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1779.75)"
      },
      {
          "fecha": "25/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "9,777",
          "tarjetaCredito": "2513.75 ",
          "tarjetaDebito": "3,738",
          "total": "16028.75 "
      },
      {
          "fecha": "24/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,318",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2318.00)"
      },
      {
          "fecha": "24/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "6,745",
          "tarjetaCredito": "1236.00 ",
          "tarjetaDebito": "12,405",
          "total": "20386.00 "
      },
      {
          "fecha": "23/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "4,293",
          "tarjetaCredito": "10803.80 ",
          "tarjetaDebito": "2,300",
          "total": "17396.80 "
      },
      {
          "fecha": "23/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,117",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2117.00)"
      },
      {
          "fecha": "22/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,039",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1039.00)"
      },
      {
          "fecha": "22/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "1,706",
          "tarjetaCredito": "3232.00 ",
          "tarjetaDebito": "5,677",
          "total": "10615.00 "
      },
      {
          "fecha": "21/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "5,786",
          "tarjetaCredito": "512.00 ",
          "tarjetaDebito": "3,481",
          "total": "9779.00 "
      },
      {
          "fecha": "21/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,674.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1674.50)"
      },
      {
          "fecha": "20/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,409.45",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2409.45)"
      },
      {
          "fecha": "20/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "3,022",
          "tarjetaCredito": "12833.45 ",
          "tarjetaDebito": "2,330",
          "total": "18185.45 "
      },
      {
          "fecha": "19/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "14.00 ",
          "efectivo": "6,723",
          "tarjetaCredito": "7450.00 ",
          "tarjetaDebito": "5,332",
          "total": "19505.00 "
      },
      {
          "fecha": "19/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,903",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1903.00)"
      },
      {
          "fecha": "18/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,007",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2007.00)"
      },
      {
          "fecha": "18/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "4,267",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "9,154",
          "total": "13421.00 "
      },
      {
          "fecha": "17/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,641",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3641.00)"
      },
      {
          "fecha": "17/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "3,667",
          "tarjetaCredito": "5497.00 ",
          "tarjetaDebito": "11,197",
          "total": "20361.00 "
      },
      {
          "fecha": "16/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "100",
          "tarjetaCredito": "3704.00 ",
          "tarjetaDebito": "6,841",
          "total": "10645.00 "
      },
      {
          "fecha": "15/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "6,797",
          "tarjetaCredito": "3190.30 ",
          "tarjetaDebito": "4,578",
          "total": "14565.30 "
      },
      {
          "fecha": "15/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,010",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3010.00)"
      },
      {
          "fecha": "14/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "1,260",
          "tarjetaCredito": "1184.00 ",
          "tarjetaDebito": "13,146",
          "total": "15590.00 "
      },
      {
          "fecha": "14/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-515",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(515.00)"
      },
      {
          "fecha": "13/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "4,354",
          "tarjetaCredito": "950.00 ",
          "tarjetaDebito": "6,728",
          "total": "12032.00 "
      },
      {
          "fecha": "13/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-2,411.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2411.50)"
      },
      {
          "fecha": "12/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,269",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1269.00)"
      },
      {
          "fecha": "12/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "3,664",
          "tarjetaCredito": "5490.00 ",
          "tarjetaDebito": "4,167",
          "total": "13321.00 "
      },
      {
          "fecha": "11/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "3,668",
          "tarjetaCredito": "3213.00 ",
          "tarjetaDebito": "5,578",
          "total": "12459.00 "
      },
      {
          "fecha": "11/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,313",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1313.00)"
      },
      {
          "fecha": "10/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-2,486.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2486.50)"
      },
      {
          "fecha": "10/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "26.00 ",
          "efectivo": "8,570",
          "tarjetaCredito": "5898.00 ",
          "tarjetaDebito": "11,355",
          "total": "25823.00 "
      },
      {
          "fecha": "09/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-411",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(411.00)"
      },
      {
          "fecha": "09/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "1,285",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,904",
          "total": "4189.00 "
      },
      {
          "fecha": "08/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "3,490",
          "tarjetaCredito": "1774.00 ",
          "tarjetaDebito": "3,550",
          "total": "8814.00 "
      },
      {
          "fecha": "08/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-2,204.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2204.50)"
      },
      {
          "fecha": "07/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "2,640",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,045",
          "total": "4685.00 "
      },
      {
          "fecha": "07/07/2020",
          "concepto": "ENTRADA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "1,061",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "1061.00 "
      },
      {
          "fecha": "07/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-1,891",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1891.00)"
      },
      {
          "fecha": "06/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,087.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1087.50)"
      },
      {
          "fecha": "06/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "6,459",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,468",
          "total": "9927.00 "
      },
      {
          "fecha": "05/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "1,500",
          "tarjetaCredito": "1935.00 ",
          "tarjetaDebito": "2,026",
          "total": "5461.00 "
      },
      {
          "fecha": "05/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,019",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1019.00)"
      },
      {
          "fecha": "04/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "5,734",
          "tarjetaCredito": "1807.00 ",
          "tarjetaDebito": "7,806.5",
          "total": "15347.50 "
      },
      {
          "fecha": "04/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,266.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3266.50)"
      },
      {
          "fecha": "03/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "8.00 ",
          "efectivo": "-4,429",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4429.00)"
      },
      {
          "fecha": "03/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "7,824",
          "tarjetaCredito": "2535.00 ",
          "tarjetaDebito": "12,487",
          "total": "22846.00 "
      },
      {
          "fecha": "02/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-1,005",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1005.00)"
      },
      {
          "fecha": "02/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "1,324",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,246",
          "total": "3570.00 "
      },
      {
          "fecha": "01/07/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "5,000",
          "tarjetaCredito": "3668.00 ",
          "tarjetaDebito": "2,632",
          "total": "11300.00 "
      },
      {
          "fecha": "01/07/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-1,958.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1958.50)"
      },
      {
          "fecha": "30/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "3,723",
          "tarjetaCredito": "8582.00 ",
          "tarjetaDebito": "1,284",
          "total": "13589.00 "
      },
      {
          "fecha": "30/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-3,722.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3722.50)"
      },
      {
          "fecha": "29/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,509",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1509.00)"
      },
      {
          "fecha": "29/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "4,210",
          "tarjetaCredito": "1182.00 ",
          "tarjetaDebito": "7,882",
          "total": "13274.00 "
      },
      {
          "fecha": "28/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "5,250",
          "tarjetaCredito": "2305.00 ",
          "tarjetaDebito": "4,055",
          "total": "11610.00 "
      },
      {
          "fecha": "28/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-992",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(992.00)"
      },
      {
          "fecha": "27/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "4,300",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,612.5",
          "total": "7912.50 "
      },
      {
          "fecha": "27/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,002",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1002.00)"
      },
      {
          "fecha": "26/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,091",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2091.00)"
      },
      {
          "fecha": "26/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "2,790",
          "tarjetaCredito": "9740.00 ",
          "tarjetaDebito": "4,911",
          "total": "17441.00 "
      },
      {
          "fecha": "25/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "9.00 ",
          "efectivo": "-4,123",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4123.00)"
      },
      {
          "fecha": "25/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "9,220",
          "tarjetaCredito": "1544.00 ",
          "tarjetaDebito": "798",
          "total": "11562.00 "
      },
      {
          "fecha": "24/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,193.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1193.50)"
      },
      {
          "fecha": "24/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "6,073",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "7,255",
          "total": "13328.00 "
      },
      {
          "fecha": "23/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,853",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2853.00)"
      },
      {
          "fecha": "23/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "3,150",
          "tarjetaCredito": "3222.00 ",
          "tarjetaDebito": "10,429",
          "total": "16801.00 "
      },
      {
          "fecha": "22/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-826.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(826.50)"
      },
      {
          "fecha": "22/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "900",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,604",
          "total": "5504.00 "
      },
      {
          "fecha": "21/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,033",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3033.00)"
      },
      {
          "fecha": "21/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "28.00 ",
          "efectivo": "13,230",
          "tarjetaCredito": "7275.00 ",
          "tarjetaDebito": "10,043",
          "total": "30548.00 "
      },
      {
          "fecha": "20/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "4,625",
          "tarjetaCredito": "1777.00 ",
          "tarjetaDebito": "8,088",
          "total": "14490.00 "
      },
      {
          "fecha": "20/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-1,887.95",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1887.95)"
      },
      {
          "fecha": "19/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,127.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1127.50)"
      },
      {
          "fecha": "19/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "4,240",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,802",
          "total": "8042.00 "
      },
      {
          "fecha": "18/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,511.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2511.50)"
      },
      {
          "fecha": "18/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "3,600",
          "tarjetaCredito": "2127.00 ",
          "tarjetaDebito": "8,006",
          "total": "13733.00 "
      },
      {
          "fecha": "17/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-3,737.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3737.50)"
      },
      {
          "fecha": "17/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "7,390",
          "tarjetaCredito": "7663.00 ",
          "tarjetaDebito": "4,736",
          "total": "19789.00 "
      },
      {
          "fecha": "16/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "1.00 ",
          "efectivo": "0",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "200",
          "total": "200.00 "
      },
      {
          "fecha": "15/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "1,876",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,087",
          "total": "5963.00 "
      },
      {
          "fecha": "15/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-811",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(811.00)"
      },
      {
          "fecha": "14/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "4,299",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,605.5",
          "total": "8904.50 "
      },
      {
          "fecha": "14/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-1,680.8",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1680.80)"
      },
      {
          "fecha": "13/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-2,614.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2614.50)"
      },
      {
          "fecha": "13/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "8,120",
          "tarjetaCredito": "250.00 ",
          "tarjetaDebito": "3,271.5",
          "total": "11641.50 "
      },
      {
          "fecha": "12/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "17.00 ",
          "efectivo": "9,020",
          "tarjetaCredito": "1818.00 ",
          "tarjetaDebito": "11,216.5",
          "total": "22054.50 "
      },
      {
          "fecha": "12/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-2,828.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2828.50)"
      },
      {
          "fecha": "11/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "4,238",
          "tarjetaCredito": "2945.00 ",
          "tarjetaDebito": "2,605",
          "total": "9788.00 "
      },
      {
          "fecha": "11/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-778",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(778.00)"
      },
      {
          "fecha": "10/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-960",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(960.00)"
      },
      {
          "fecha": "10/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,970",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,705",
          "total": "4675.00 "
      },
      {
          "fecha": "09/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "1,165",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,748",
          "total": "6913.00 "
      },
      {
          "fecha": "09/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-946",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(946.00)"
      },
      {
          "fecha": "08/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "2.00 ",
          "efectivo": "435",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,956",
          "total": "3391.00 "
      },
      {
          "fecha": "08/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-355.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(355.50)"
      },
      {
          "fecha": "07/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-841",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(841.00)"
      },
      {
          "fecha": "07/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "3,893",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,677",
          "total": "7570.00 "
      },
      {
          "fecha": "06/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(400.00)"
      },
      {
          "fecha": "06/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,100",
          "tarjetaCredito": "1177.00 ",
          "tarjetaDebito": "129",
          "total": "2406.00 "
      },
      {
          "fecha": "05/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "3,448.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,492",
          "total": "7940.50 "
      },
      {
          "fecha": "05/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,024",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1024.00)"
      },
      {
          "fecha": "04/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "1,900",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,658",
          "total": "3558.00 "
      },
      {
          "fecha": "04/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-585",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(585.00)"
      },
      {
          "fecha": "03/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,957.75",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1957.75)"
      },
      {
          "fecha": "03/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "4,300",
          "tarjetaCredito": "3521.20 ",
          "tarjetaDebito": "4,491.55",
          "total": "12312.75 "
      },
      {
          "fecha": "02/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "2,464.2",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "888",
          "total": "3352.20 "
      },
      {
          "fecha": "02/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "5.00 ",
          "efectivo": "-688.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(688.50)"
      },
      {
          "fecha": "01/06/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "5,938",
          "tarjetaCredito": "3793.00 ",
          "tarjetaDebito": "7,012",
          "total": "16743.00 "
      },
      {
          "fecha": "01/06/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-2,573.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2573.50)"
      },
      {
          "fecha": "31/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,954",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1954.00)"
      },
      {
          "fecha": "31/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "5,190",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "11,496",
          "total": "16686.00 "
      },
      {
          "fecha": "30/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "6,395",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,409",
          "total": "9804.00 "
      },
      {
          "fecha": "30/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,236",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1236.00)"
      },
      {
          "fecha": "29/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-763",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(763.00)"
      },
      {
          "fecha": "29/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "4,713.2",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,595",
          "total": "6308.20 "
      },
      {
          "fecha": "28/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "3,426",
          "tarjetaCredito": "980.95 ",
          "tarjetaDebito": "310",
          "total": "4716.95 "
      },
      {
          "fecha": "28/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-923.95",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(923.95)"
      },
      {
          "fecha": "27/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-250",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(250.00)"
      },
      {
          "fecha": "27/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "250",
          "tarjetaCredito": "1773.00 ",
          "tarjetaDebito": "1,479",
          "total": "3502.00 "
      },
      {
          "fecha": "26/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,005",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1005.00)"
      },
      {
          "fecha": "26/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "3,830",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,418",
          "total": "9248.00 "
      },
      {
          "fecha": "25/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,265",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,882",
          "total": "5147.00 "
      },
      {
          "fecha": "25/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-655",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(655.00)"
      },
      {
          "fecha": "24/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,934",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1934.00)"
      },
      {
          "fecha": "24/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "3,570",
          "tarjetaCredito": "720.00 ",
          "tarjetaDebito": "7,986",
          "total": "12276.00 "
      },
      {
          "fecha": "23/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,372.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1372.50)"
      },
      {
          "fecha": "23/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "3,185",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,786.9",
          "total": "6971.90 "
      },
      {
          "fecha": "22/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,314",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2314.00)"
      },
      {
          "fecha": "22/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "2,750",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "7,549",
          "total": "10299.00 "
      },
      {
          "fecha": "21/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "1,260",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,396",
          "total": "6656.00 "
      },
      {
          "fecha": "21/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,238",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1238.00)"
      },
      {
          "fecha": "20/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "5,580",
          "tarjetaCredito": "1454.00 ",
          "tarjetaDebito": "2,435",
          "total": "9469.00 "
      },
      {
          "fecha": "20/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "6.00 ",
          "efectivo": "-1,407",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1407.00)"
      },
      {
          "fecha": "19/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "1.00 ",
          "efectivo": "800",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "800.00 "
      },
      {
          "fecha": "18/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "2,876",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "9,812.8",
          "total": "12688.80 "
      },
      {
          "fecha": "18/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,437",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1437.00)"
      },
      {
          "fecha": "17/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "5,200",
          "tarjetaCredito": "600.00 ",
          "tarjetaDebito": "2,883",
          "total": "8683.00 "
      },
      {
          "fecha": "17/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-944",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(944.00)"
      },
      {
          "fecha": "16/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-600",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(600.00)"
      },
      {
          "fecha": "16/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "3,720",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,129.9",
          "total": "6849.90 "
      },
      {
          "fecha": "15/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "4,921.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,876",
          "total": "7797.50 "
      },
      {
          "fecha": "15/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,125",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4125.00)"
      },
      {
          "fecha": "14/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,404",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1404.00)"
      },
      {
          "fecha": "14/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "2.00 ",
          "efectivo": "4,000",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "4000.00 "
      },
      {
          "fecha": "13/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "40",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "8,681",
          "total": "8721.00 "
      },
      {
          "fecha": "12/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,957",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2957.00)"
      },
      {
          "fecha": "12/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "4,320",
          "tarjetaCredito": "2245.00 ",
          "tarjetaDebito": "11,999",
          "total": "18564.00 "
      },
      {
          "fecha": "11/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "2,050",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,810",
          "total": "6860.00 "
      },
      {
          "fecha": "11/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,689",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1689.00)"
      },
      {
          "fecha": "10/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "8.00 ",
          "efectivo": "-44,556",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(44556.00)"
      },
      {
          "fecha": "10/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "64.00 ",
          "efectivo": "47,391.5",
          "tarjetaCredito": "19622.70 ",
          "tarjetaDebito": "18,582.65",
          "total": "85596.85 "
      },
      {
          "fecha": "09/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "9,908.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "11,101",
          "total": "21009.50 "
      },
      {
          "fecha": "09/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-5,969",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(5969.00)"
      },
      {
          "fecha": "08/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,837",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4837.00)"
      },
      {
          "fecha": "08/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "5,070",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,672",
          "total": "9742.00 "
      },
      {
          "fecha": "07/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-485",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(485.00)"
      },
      {
          "fecha": "07/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "485",
          "tarjetaCredito": "1787.10 ",
          "tarjetaDebito": "2,688",
          "total": "4960.10 "
      },
      {
          "fecha": "06/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "2.00 ",
          "efectivo": "250",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "284",
          "total": "534.00 "
      },
      {
          "fecha": "06/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-250",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(250.00)"
      },
      {
          "fecha": "05/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "1.00 ",
          "efectivo": "0",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "165",
          "total": "165.00 "
      },
      {
          "fecha": "04/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(400.00)"
      },
      {
          "fecha": "04/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,061.5",
          "tarjetaCredito": "1200.00 ",
          "tarjetaDebito": "0",
          "total": "2261.50 "
      },
      {
          "fecha": "03/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-839.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(839.50)"
      },
      {
          "fecha": "03/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "1,500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,441.2",
          "total": "3941.20 "
      },
      {
          "fecha": "02/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(500.00)"
      },
      {
          "fecha": "02/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,559",
          "total": "5059.00 "
      },
      {
          "fecha": "01/05/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "4,929",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "792",
          "total": "5721.00 "
      },
      {
          "fecha": "01/05/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-1,506",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1506.00)"
      },
      {
          "fecha": "30/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "10.00 ",
          "efectivo": "6,992",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,919",
          "total": "12911.00 "
      },
      {
          "fecha": "30/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,578",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2578.00)"
      },
      {
          "fecha": "29/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "520",
          "tarjetaCredito": "2765.00 ",
          "tarjetaDebito": "2,293",
          "total": "5578.00 "
      },
      {
          "fecha": "29/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-520",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(520.00)"
      },
      {
          "fecha": "28/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-657.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(657.50)"
      },
      {
          "fecha": "28/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "1.00 ",
          "efectivo": "1,200",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "1200.00 "
      },
      {
          "fecha": "27/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-677",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(677.00)"
      },
      {
          "fecha": "27/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "2,469",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,468",
          "total": "5937.00 "
      },
      {
          "fecha": "26/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "13.00 ",
          "efectivo": "4,958.9",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "4,301",
          "total": "9259.90 "
      },
      {
          "fecha": "26/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-4,515",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4515.00)"
      },
      {
          "fecha": "25/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "5,350",
          "tarjetaCredito": "2072.00 ",
          "tarjetaDebito": "1,561",
          "total": "8983.00 "
      },
      {
          "fecha": "25/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-645.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(645.50)"
      },
      {
          "fecha": "24/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,805",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "1805.00 "
      },
      {
          "fecha": "24/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,198",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1198.00)"
      },
      {
          "fecha": "23/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,172",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1172.00)"
      },
      {
          "fecha": "23/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "3,550",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,049",
          "total": "6599.00 "
      },
      {
          "fecha": "22/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(500.00)"
      },
      {
          "fecha": "22/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "826.7",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,672",
          "total": "3498.70 "
      },
      {
          "fecha": "21/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "2,180",
          "tarjetaCredito": "4567.00 ",
          "tarjetaDebito": "1,074",
          "total": "7821.00 "
      },
      {
          "fecha": "21/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-305",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(305.00)"
      },
      {
          "fecha": "20/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "1,270",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,431",
          "total": "4701.00 "
      },
      {
          "fecha": "20/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-763.5",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(763.50)"
      },
      {
          "fecha": "19/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "4,883",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,838",
          "total": "7721.00 "
      },
      {
          "fecha": "19/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-558",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(558.00)"
      },
      {
          "fecha": "18/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "890",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,114",
          "total": "3004.00 "
      },
      {
          "fecha": "18/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-527",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(527.00)"
      },
      {
          "fecha": "17/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1500.00)"
      },
      {
          "fecha": "17/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "3,274",
          "tarjetaCredito": "1066.00 ",
          "tarjetaDebito": "3,208",
          "total": "7548.00 "
      },
      {
          "fecha": "16/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,339",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1339.00)"
      },
      {
          "fecha": "16/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "1,400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,718",
          "total": "4118.00 "
      },
      {
          "fecha": "15/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(400.00)"
      },
      {
          "fecha": "15/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "400",
          "tarjetaCredito": "1765.00 ",
          "tarjetaDebito": "2,923",
          "total": "5088.00 "
      },
      {
          "fecha": "14/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "3,900",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "5,886",
          "total": "9786.00 "
      },
      {
          "fecha": "14/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,233",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2233.00)"
      },
      {
          "fecha": "13/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,029",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1029.00)"
      },
      {
          "fecha": "13/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "1,475",
          "tarjetaCredito": "6989.00 ",
          "tarjetaDebito": "0",
          "total": "8464.00 "
      },
      {
          "fecha": "12/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "2,320.5",
          "tarjetaCredito": "1641.00 ",
          "tarjetaDebito": "2,298",
          "total": "6259.50 "
      },
      {
          "fecha": "12/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-2,100",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(2100.00)"
      },
      {
          "fecha": "11/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "2,249",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,997",
          "total": "5246.00 "
      },
      {
          "fecha": "11/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-498",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(498.00)"
      },
      {
          "fecha": "10/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "375",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,377",
          "total": "2752.00 "
      },
      {
          "fecha": "10/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-300",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(300.00)"
      },
      {
          "fecha": "09/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-461",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(461.00)"
      },
      {
          "fecha": "09/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "500",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,991",
          "total": "3491.00 "
      },
      {
          "fecha": "08/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "285",
          "tarjetaCredito": "442.00 ",
          "tarjetaDebito": "1,526",
          "total": "2253.00 "
      },
      {
          "fecha": "08/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-225",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(225.00)"
      },
      {
          "fecha": "07/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-562",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(562.00)"
      },
      {
          "fecha": "07/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "1,780",
          "tarjetaCredito": "2654.00 ",
          "tarjetaDebito": "250",
          "total": "4684.00 "
      },
      {
          "fecha": "06/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "3,670",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,741",
          "total": "5411.00 "
      },
      {
          "fecha": "06/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,021",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1021.00)"
      },
      {
          "fecha": "05/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "3,202",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,111",
          "total": "4313.00 "
      },
      {
          "fecha": "05/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,240",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1240.00)"
      },
      {
          "fecha": "04/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "1,050",
          "tarjetaCredito": "772.00 ",
          "tarjetaDebito": "0",
          "total": "1822.00 "
      },
      {
          "fecha": "04/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-411",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(411.00)"
      },
      {
          "fecha": "03/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "4.00 ",
          "efectivo": "600",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,699",
          "total": "3299.00 "
      },
      {
          "fecha": "03/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-394",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(394.00)"
      },
      {
          "fecha": "02/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "2,038",
          "tarjetaCredito": "929.00 ",
          "tarjetaDebito": "0",
          "total": "2967.00 "
      },
      {
          "fecha": "02/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-790",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(790.00)"
      },
      {
          "fecha": "01/04/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-1,495",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1495.00)"
      },
      {
          "fecha": "01/04/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "1,495",
          "tarjetaCredito": "4634.00 ",
          "tarjetaDebito": "6,832",
          "total": "12961.00 "
      },
      {
          "fecha": "31/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "5.00 ",
          "efectivo": "7,390",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "3,562",
          "total": "10952.00 "
      },
      {
          "fecha": "31/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,304",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1304.00)"
      },
      {
          "fecha": "29/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "2.00 ",
          "efectivo": "460",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "100",
          "total": "560.00 "
      },
      {
          "fecha": "29/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-460",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(460.00)"
      },
      {
          "fecha": "28/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-249",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(249.00)"
      },
      {
          "fecha": "28/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "7.00 ",
          "efectivo": "2,637",
          "tarjetaCredito": "2150.00 ",
          "tarjetaDebito": "1,021",
          "total": "5808.00 "
      },
      {
          "fecha": "27/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-885",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(885.00)"
      },
      {
          "fecha": "27/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "3.00 ",
          "efectivo": "885",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "2,139",
          "total": "3024.00 "
      },
      {
          "fecha": "26/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "9.00 ",
          "efectivo": "1,349",
          "tarjetaCredito": "5142.00 ",
          "tarjetaDebito": "5,517",
          "total": "12008.00 "
      },
      {
          "fecha": "25/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-635",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(635.00)"
      },
      {
          "fecha": "25/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "4,935",
          "tarjetaCredito": "854.00 ",
          "tarjetaDebito": "4,940",
          "total": "10729.00 "
      },
      {
          "fecha": "24/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-3,696",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3696.00)"
      },
      {
          "fecha": "24/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "8.00 ",
          "efectivo": "4,029",
          "tarjetaCredito": "4362.00 ",
          "tarjetaDebito": "519",
          "total": "8910.00 "
      },
      {
          "fecha": "23/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-239",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(239.00)"
      },
      {
          "fecha": "23/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "1,786",
          "tarjetaCredito": "3378.00 ",
          "tarjetaDebito": "2,186",
          "total": "7350.00 "
      },
      {
          "fecha": "22/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-783",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(783.00)"
      },
      {
          "fecha": "22/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "10,445",
          "tarjetaCredito": "12524.00 ",
          "tarjetaDebito": "7,216.05",
          "total": "30185.05 "
      },
      {
          "fecha": "21/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-615",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(615.00)"
      },
      {
          "fecha": "21/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "18.00 ",
          "efectivo": "7,535",
          "tarjetaCredito": "2456.00 ",
          "tarjetaDebito": "7,882",
          "total": "17873.00 "
      },
      {
          "fecha": "20/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "2,163.66",
          "tarjetaCredito": "9245.60 ",
          "tarjetaDebito": "7,925",
          "total": "19334.26 "
      },
      {
          "fecha": "20/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,950",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1950.00)"
      },
      {
          "fecha": "19/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-237",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(237.00)"
      },
      {
          "fecha": "19/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "9,482",
          "tarjetaCredito": "7972.60 ",
          "tarjetaDebito": "10,243",
          "total": "27697.60 "
      },
      {
          "fecha": "18/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "6.00 ",
          "efectivo": "2,154",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "1,849",
          "total": "4003.00 "
      },
      {
          "fecha": "17/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-3,000",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3000.00)"
      },
      {
          "fecha": "17/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "4,769",
          "tarjetaCredito": "6129.00 ",
          "tarjetaDebito": "6,617",
          "total": "17515.00 "
      },
      {
          "fecha": "16/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "16.00 ",
          "efectivo": "12,220",
          "tarjetaCredito": "4021.00 ",
          "tarjetaDebito": "11,116",
          "total": "27357.00 "
      },
      {
          "fecha": "16/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-825",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(825.00)"
      },
      {
          "fecha": "15/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "12.00 ",
          "efectivo": "8,926",
          "tarjetaCredito": "2714.00 ",
          "tarjetaDebito": "4,635.2",
          "total": "16275.20 "
      },
      {
          "fecha": "14/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "15.00 ",
          "efectivo": "4,435",
          "tarjetaCredito": "4567.00 ",
          "tarjetaDebito": "9,826.5",
          "total": "18828.50 "
      },
      {
          "fecha": "13/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-10,400",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(10400.00)"
      },
      {
          "fecha": "13/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "26.00 ",
          "efectivo": "11,459",
          "tarjetaCredito": "18255.00 ",
          "tarjetaDebito": "12,476.5",
          "total": "42190.50 "
      },
      {
          "fecha": "12/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-4,475",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(4475.00)"
      },
      {
          "fecha": "12/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "40.00 ",
          "efectivo": "9,454",
          "tarjetaCredito": "18611.00 ",
          "tarjetaDebito": "9,669",
          "total": "37734.00 "
      },
      {
          "fecha": "11/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "19.00 ",
          "efectivo": "9,738",
          "tarjetaCredito": "9359.70 ",
          "tarjetaDebito": "5,125.7",
          "total": "24223.40 "
      },
      {
          "fecha": "11/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-6,000",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6000.00)"
      },
      {
          "fecha": "10/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-11,487",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(11487.00)"
      },
      {
          "fecha": "10/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "12,929",
          "tarjetaCredito": "8868.00 ",
          "tarjetaDebito": "11,070.9",
          "total": "32867.90 "
      },
      {
          "fecha": "09/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "2.00 ",
          "efectivo": "-1,095",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1095.00)"
      },
      {
          "fecha": "09/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "30.00 ",
          "efectivo": "8,487",
          "tarjetaCredito": "24562.45 ",
          "tarjetaDebito": "9,443.8",
          "total": "42493.25 "
      },
      {
          "fecha": "08/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "48.00 ",
          "efectivo": "18,585",
          "tarjetaCredito": "38054.10 ",
          "tarjetaDebito": "10,079",
          "total": "66718.10 "
      },
      {
          "fecha": "08/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "7.00 ",
          "efectivo": "-17,441",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(17441.00)"
      },
      {
          "fecha": "07/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "38.00 ",
          "efectivo": "12,745",
          "tarjetaCredito": "11595.48 ",
          "tarjetaDebito": "13,958",
          "total": "38298.48 "
      },
      {
          "fecha": "07/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-899",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(899.00)"
      },
      {
          "fecha": "06/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,833",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3833.00)"
      },
      {
          "fecha": "06/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "32.00 ",
          "efectivo": "4,943",
          "tarjetaCredito": "33034.35 ",
          "tarjetaDebito": "13,087",
          "total": "51064.35 "
      },
      {
          "fecha": "05/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "20.00 ",
          "efectivo": "9,234",
          "tarjetaCredito": "10502.00 ",
          "tarjetaDebito": "13,910",
          "total": "33646.00 "
      },
      {
          "fecha": "05/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-6,524",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(6524.00)"
      },
      {
          "fecha": "04/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "9,145",
          "tarjetaCredito": "9626.70 ",
          "tarjetaDebito": "5,776",
          "total": "24547.70 "
      },
      {
          "fecha": "04/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-1,010",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(1010.00)"
      },
      {
          "fecha": "03/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "32.00 ",
          "efectivo": "10,946",
          "tarjetaCredito": "15716.55 ",
          "tarjetaDebito": "17,045",
          "total": "43707.55 "
      },
      {
          "fecha": "03/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "4.00 ",
          "efectivo": "-3,544",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(3544.00)"
      },
      {
          "fecha": "02/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "1.00 ",
          "efectivo": "-319",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(319.00)"
      },
      {
          "fecha": "02/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "11.00 ",
          "efectivo": "1,846",
          "tarjetaCredito": "9387.00 ",
          "tarjetaDebito": "2,731",
          "total": "13964.00 "
      },
      {
          "fecha": "01/03/2020",
          "concepto": "SALIDA_CAJA",
          "totalTransacciones": "3.00 ",
          "efectivo": "-9,600",
          "tarjetaCredito": "0.00 ",
          "tarjetaDebito": "0",
          "total": "(9600.00)"
      },
      {
          "fecha": "01/03/2020",
          "concepto": "VENTA",
          "totalTransacciones": "24.00 ",
          "efectivo": "14,799",
          "tarjetaCredito": "7072.95 ",
          "tarjetaDebito": "11,589.2",
          "total": "33461.15 "
      }
  ]
