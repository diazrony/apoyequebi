export const dataVCategoria = [
  {
    "name": "EXTRAS",
    "value": 490463.868894
  },
  {
    "name": "ALIMENTOS",
    "value": 2832604.952704
  },
  {
    "name": "POSTRES",
    "value": 155193.976218
  },
  {
    "name": "BOTELLA",
    "value": 287968.103478
  },
  {
    "name": "TRAGOS",
    "value": 692231.033832
  },
  {
    "name": "MODIFICADOR ALIMENTOS",
    "value": 344.827588
  },
  {
    "name": "BEBIDAS",
    "value": 707026.739666
  }
];
